﻿using UnityEngine;
using System.Collections;

using FullInspector;
using Mercury.Util.Security;
using Mercury.Util.Storage;

namespace Mercury {

	[CreateAssetMenu]
	public class MercuryConfiguration : BaseScriptableObject {

		public static MercuryConfiguration Instance { get { return Resources.Load<MercuryConfiguration> ("MercuryConfiguration"); } }

		[SerializeField]
		IStorageService m_storageService;
		public IStorageService StorageService { get { return m_storageService; } }

		[SerializeField]
		IAntiCheatService m_antiCheatService;
		public IAntiCheatService AntiCheatService { get { return m_antiCheatService; } }

	}

}
