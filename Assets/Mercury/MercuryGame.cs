﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

using Mercury.Messaging;
using Mercury.Helper;

namespace Mercury {
	
	public abstract class MercuryGame : SingletonComponent<MercuryGame> {
		private static bool s_isAlive = true;
		public static MercuryGame Instance { get { return s_isAlive ? (MercuryGame)_Instance : null; } }

		#region EVENTS

		public event System.Action onStartGame;
		public event System.Func<IEnumerator> onStartGameAsync;
		public event System.Action onEndGame;
		public event System.Func<IEnumerator> onEndGameAsync;

		public event System.Action<bool> onApplicationFocus;

		#endregion

		#region PUBLIC FIELDS & PROPERTIES

		public int targetFrameRate = 60;

		public IGameMode ActiveGameMode { get; protected set; }

		#endregion

		public abstract void Init (System.Action callback);
		public abstract void Unload (System.Action callback);

		#region MONOBEHAVIOUR MESSAGES

		protected virtual void Start () {
			Application.targetFrameRate = targetFrameRate;

			Messenger.Instance.Subscribe<StartGameMessage> (HandleStartGame);
		}

		protected virtual void OnDestroy () {
			s_isAlive = false;
		}

		protected virtual void OnApplicationFocus (bool isFocus) {
			if (!isFocus && ActiveGameMode != null && ActiveGameMode.AutoPause)
				ActiveGameMode.PauseGame ();

			if (onApplicationFocus != null)
				onApplicationFocus (isFocus);
		}

		protected virtual void OnApplicationQuit () {
			s_isAlive = false;
		}

		#endregion

		#region CALLBACK HANDLING

		Coroutine InvokeOnStartGame () {
			if (onStartGame != null)
				onStartGame ();
			
			if (onStartGameAsync != null)
				return StartCoroutine (onStartGameAsync ());
			
			return null;
		}
		
		Coroutine InvokeOnEndGame () {
			if (onEndGame != null)
				onEndGame ();
			
			if (onEndGameAsync != null)
				return StartCoroutine (onEndGameAsync ());
			
			return null;
		}

		bool HandleStartGame (BaseMessage msg) {
			var sgm = (StartGameMessage)msg;
			StartGame (sgm.gameMode);
			
			return false;
		}
		
		IEnumerator _StartGame (params object[] parameter) {
			ActiveGameMode.PreLoad ();
			
			yield return InvokeOnStartGame ();
			
			ActiveGameMode.StartGame (parameter);
		}

		#endregion

		public void StartGame (IGameMode gameMode, params object[] parameter) {
			ActiveGameMode = gameMode;
			StartGame (parameter);
		}

		public void StartGame (params object[] parameter) {
			if (ActiveGameMode != null)
				MercuryScene.ChangeScene (ActiveGameMode.SceneName, _StartGame, parameter);
		}
		
		public void EndGame () {
			if (ActiveGameMode != null) {
				ActiveGameMode.EndGame ();
				InvokeOnEndGame ();
			}
		}

		public abstract void ResetData ();
	}

}