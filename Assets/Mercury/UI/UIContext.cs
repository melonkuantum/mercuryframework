﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Mercury.UI {

	public static class UIContext {
		static Dictionary<string, System.Action<object>> m_onChangeListeners = new Dictionary<string, System.Action<object>> ();
		static Dictionary<string, object> m_dataMap = new Dictionary<string, object> ();

		public static void Store (string key, object value, bool forceChange = false) {
			if (string.IsNullOrEmpty (key))
				return;

			var isChanged = false;

			if (m_dataMap.ContainsKey (key)) {
				if (m_dataMap [key] == null ||
				    !m_dataMap [key].Equals (value)) {
					m_dataMap [key] = value;
					isChanged = true;
				}
			} else {
				m_dataMap.Add (key, value);
				isChanged = true;
			}

			if ((isChanged || forceChange) &&
			    m_onChangeListeners.ContainsKey (key) &&
				m_onChangeListeners [key] != null)
				m_onChangeListeners [key] (value);
		}

		public static void Remove (string key) {
			if (string.IsNullOrEmpty (key))
				return;

			m_dataMap.Remove (key);
		}

		public static object Get (string key) {
			if (string.IsNullOrEmpty (key))
				return null;

			object value = null;
			m_dataMap.TryGetValue (key, out value);

			return value;
		}

		public static T Get<T> (string key) {
			if (string.IsNullOrEmpty (key))
				return default (T);

			var value = Get (key);

			try {
				return (T)value;
			} catch {
				return default (T);
			}
		}

		public static void Register (string key, System.Action<object> listener) {
			if (string.IsNullOrEmpty (key))
				return;

			if (m_onChangeListeners.ContainsKey (key)) {
				if (m_onChangeListeners [key] == null)
					m_onChangeListeners [key] = listener;
				else
					m_onChangeListeners [key] += listener;
			} else {
				m_onChangeListeners.Add (key, listener);
			}
		}

		public static void UnRegister (string key, System.Action<object> listener) {
			if (string.IsNullOrEmpty (key))
				return;

			if (m_onChangeListeners.ContainsKey (key) &&
			    m_onChangeListeners [key] != null) {
				m_onChangeListeners [key] -= listener;
			}
		}
	}

}