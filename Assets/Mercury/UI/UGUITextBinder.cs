﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Mercury.UI {

	[RequireComponent (typeof (Text))]
	public class UGUITextBinder : UIBinder {
		public Text target;
		public string defaultText = "NULL";

		void Awake () {
			if (target == null)
				target = GetComponent<Text> ();
		}

		public override void Bind () {
			var value = UIContext.Get (key);
			target.text = value == null ? defaultText : value.ToString ();
		}
	}

}