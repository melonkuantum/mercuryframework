﻿using UnityEngine;
using System.Collections;

namespace Mercury.UI {

	public interface IBackButtonResponder {
		object Responder { get; }
		int Depth { get; set; }
		
		void RespondToBackButton ();
	}

}