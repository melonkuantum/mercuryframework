﻿using UnityEngine;
using System.Collections;

namespace Mercury.UI {
	
	public class GameObjectStatusBinder : UIBinder {

		public GameObject target;
		public bool invert;

		public override void Bind () {
			var value = UIContext.Get<bool> (key);
			target.SetActive (invert ? !value : value);
		}

	}

}