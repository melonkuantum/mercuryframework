﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

namespace Mercury.UI {

	public class UGUIManager : UIManager {
		public bool multiTouch;

		IAnimationResolver m_animationResolver;
		public override IAnimationResolver AnimationResolver { get { return m_animationResolver; } }

		EventSystem m_eventSystem;

		protected override void Init () {
			Input.multiTouchEnabled = multiTouch;
			m_animationResolver = new AnimatorResolver ();
		}

		protected override void DoWhenStatic () {
			EnableInput ();
		}

		protected override void DoWhenAnimating () {
			DisableInput ();
		}

		protected override BaseMenu CreateMenu (BaseMenu menuPrefab) {
			var menu = Instantiate (menuPrefab);
			menu.transform.parent = root;
			menu.transform.localScale = Vector3.one;

			if (menu.transform is RectTransform) {
				var rct = (RectTransform)menu.transform;
				rct.offsetMin = Vector2.zero;
				rct.offsetMax = Vector2.zero;
			}
			
			return menu;
		}

		public void EnableInput () {
			if (m_eventSystem != null &&
			    !m_eventSystem.enabled) {
				m_eventSystem.enabled = true;
				EventSystem.current = m_eventSystem;
			}
		}

		public void DisableInput () {
			if (EventSystem.current != null &&
				EventSystem.current.enabled) {
				m_eventSystem = EventSystem.current;
				m_eventSystem.enabled = false;
			}
		}
	}

}