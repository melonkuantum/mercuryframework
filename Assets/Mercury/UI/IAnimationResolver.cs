﻿using UnityEngine;
using System.Collections;

namespace Mercury.UI {

	public interface IAnimationResolver {
		bool IsAnimating ();
		void TransIn (GameObject obj, System.Action<GameObject> callback = null);
		void TransOut (GameObject obj, System.Action<GameObject> callback = null);
	}

}