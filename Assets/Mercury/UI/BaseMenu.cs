using UnityEngine;
using System.Collections;

using Mercury.Messaging;

namespace Mercury.UI {

	public class BaseMenu : MonoBehaviour, IBackButtonResponder {
		public bool closeOnDefault = true;
		public bool destroyOnClose;
		public bool isModal;
		public float closeDelay;

		protected bool m_readyToClose;
		protected UIManager m_uiManager;

		bool m_isInScene = false;
		public bool IsInScene { get { return m_isInScene; } }

		public virtual object Responder { get { return gameObject; } }
		public virtual int Depth { get; set; }

		protected virtual void Awake () {
			m_isInScene = true;
		}

		protected virtual void OnEnable () {
//			if (closeDelay > 0) {
//				m_readyToClose = false;
//				Coroutiner.WaitAndDoRealtime (closeDelay, delegate {
//					m_readyToClose = true;
//				});
//			} else {
				m_readyToClose = true;
//			}

			Messenger.Instance.Subscribe<BackButtonMessage> (HandleBackButton);
		}

		protected virtual void OnDisable () {
			if (Messenger.Instance != null)
				Messenger.Instance.Unsubscribe<BackButtonMessage> (HandleBackButton);
		}

		protected bool HandleBackButton (BaseMessage msg) {
			RespondToBackButton ();
			return true;
		}

		public virtual void RespondToBackButton () {
			if (closeOnDefault)
				Close ();
		}

		public virtual void Show (UIManager uiManager, System.Action<GameObject> onShow = null, params object[] parameters) {
			m_uiManager = uiManager;
			m_uiManager.AnimationResolver.TransIn (gameObject, onShow);
		}

		public virtual void Close (System.Action<GameObject> onClose = null) {
			if (m_readyToClose)
				m_uiManager.AnimationResolver.TransOut (gameObject, onClose);
		}
	}

}