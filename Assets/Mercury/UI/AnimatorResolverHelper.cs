﻿using UnityEngine;
using System.Collections;

namespace Mercury.UI {

	public class AnimatorResolverHelper : MonoBehaviour {
		public Animator[] animators;
		public bool isAnimating;

		public void TransIn () {
			for (int i = 0; i < animators.Length; i++) {
				animators[i].SetBool ("isAnimating", true);
				animators[i].Play ("TransIn", 0);
			}

			isAnimating = true;
		}

		public void TransOut () {
			for (int i = 0; i < animators.Length; i++) {
				animators[i].SetBool ("isAnimating", true);
				animators[i].Play ("TransOut", 0);
			}

			isAnimating = true;
		}

		public void Finish () {
			for (int i = 0; i < animators.Length; i++)
				animators[i].SetBool ("isAnimating", false);

			isAnimating = false;
		}
	}

}