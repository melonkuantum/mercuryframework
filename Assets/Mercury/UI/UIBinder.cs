﻿using UnityEngine;
using System.Collections;

namespace Mercury.UI {

	public abstract class UIBinder : MonoBehaviour {
		public enum BindingType { OnEnable, OnUpdate, OnChange };

		public string key;
		public BindingType bindingType = BindingType.OnUpdate;

		void OnEnable () {
			if (bindingType == BindingType.OnEnable) {
				Bind ();
			} else if (bindingType == BindingType.OnChange) {
				UIContext.Register (key, OnChange);
				Bind ();
			} else if (bindingType == BindingType.OnUpdate) {
				MercuryUpdate.onGameUpdate += OnUpdate;
			}
		}

		void OnDisable () {
			if (bindingType == BindingType.OnChange)
				UIContext.UnRegister (key, OnChange);
			else if (bindingType == BindingType.OnUpdate)
				MercuryUpdate.onGameUpdate -= OnUpdate;
		}

		void OnUpdate (float dt) {
			if (bindingType == BindingType.OnUpdate)
				Bind ();
		}

		protected void OnChange (object value) {
			Bind ();
		}

		public abstract void Bind ();
	}

}