﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Mercury.UI {

	public class UGUISelectableStatusBinder : UIBinder {
		
		public Selectable target;
		public bool invert;

		void Awake () {
			if (target == null)
				target = GetComponent<Selectable> ();
		}

		public override void Bind () {
			var value = UIContext.Get<bool> (key);
			target.interactable = invert ? !value : value;
		}

	}

}