﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Mercury.UI {

	[RequireComponent (typeof (Slider))]
	public class UGUISliderBinder : UIBinder {
		public Slider target;
		public bool invert;

		void Awake () {
			if (target == null)
				target = GetComponent<Slider> ();
		}
		
		public override void Bind () {
			var value = UIContext.Get<float> (key);

			if (invert)
				value = 1 - value;

			target.normalizedValue = value;
		}
	}

}