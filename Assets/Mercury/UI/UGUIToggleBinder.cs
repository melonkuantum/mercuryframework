﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Mercury.UI {

	[RequireComponent (typeof (Toggle))]
	public class UGUIToggleBinder : UIBinder {
		public Toggle target;

		void Awake () {
			if (target == null)
				target = GetComponent<Toggle> ();
		}
		
		public override void Bind () {
			var value = UIContext.Get<bool> (key);
			target.isOn = value;
		}
	}

}