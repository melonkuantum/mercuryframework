﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Mercury.Messaging;
using Mercury.Helper;

namespace Mercury.UI {

	public abstract class UIManager : MonoBehaviour {
		
		public abstract IAnimationResolver AnimationResolver { get; }

		public Transform root;

		protected BaseMenu m_activeMenu;
		public BaseMenu ActiveMenu { get { return m_activeMenu; } }

		protected virtual void Awake () {
			Init ();
			InitRouting ();
		}

		void Update () {
			if (AnimationResolver.IsAnimating ())
				DoWhenAnimating ();
			else
				DoWhenStatic ();
		}

		protected virtual void Init () {
		}

		protected virtual void InitRouting () {
			Messenger.Instance.Subscribe<GoToMenuMessage> (HandleGoToMenu);
		}

		protected abstract void DoWhenStatic ();
		protected abstract void DoWhenAnimating ();

		protected bool HandleGoToMenu (BaseMessage msg) {
			var gtmMsg = (GoToMenuMessage)msg;
			Show (gtmMsg.targetMenu, gtmMsg.onShow, gtmMsg.parameters);

			return true;
		}

		protected void ProcessBackButton () {
			if (Input.GetKeyUp (KeyCode.Escape))
				Messenger.Instance.QueueMessage (BackButtonMessage.Instance);
		}

		protected virtual BaseMenu CreateMenu (BaseMenu menuPrefab) {
			var menu = Instantiate (menuPrefab);
			menu.transform.parent = root;
			menu.transform.localScale = Vector3.one;

			return menu;
		}

		protected void SetActive (BaseMenu menuPrefab, System.Action<GameObject> onShow = null, params object[] parameters) {
			BaseMenu menu = null;

			if (menuPrefab.IsInScene)
				menu = menuPrefab;
			else
				menu = CreateMenu (menuPrefab);

			if (!menu.isModal)
				m_activeMenu = menu;

			menu.Show (this, onShow, parameters);
		}

		public void Show (BaseMenu menuPrefab, System.Action<GameObject> onShow = null, params object[] parameters) {
			if (!menuPrefab.isModal &&
			    m_activeMenu != null &&
			    m_activeMenu != menuPrefab)
				m_activeMenu.Close (delegate {
					SetActive (menuPrefab, onShow, parameters);
				});
			else
				SetActive (menuPrefab, onShow, parameters);
		}
	}

}