﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Mercury.UI {

	public class AnimatorResolver : IAnimationResolver {
		Dictionary<AnimatorResolverHelper, System.Action<GameObject>> m_forwardCallbackMap;
		Dictionary<AnimatorResolverHelper, System.Action<GameObject>> m_backwardCallbackMap;

		void PerformForwardCallback (GameObject obj, System.Action<GameObject> callback) {
			if (callback != null)
				callback (obj);
		}

		void PerformBackwardCallback (GameObject obj, System.Action<GameObject> callback) {
			if (callback != null)
				callback (obj);

			var bm = obj.GetComponent<BaseMenu> ();
			if (bm != null) {
				if (bm.destroyOnClose)
					GameObject.Destroy (obj);
				else
					obj.SetActive (false);
			}
		}

		public AnimatorResolver () {
			m_forwardCallbackMap = new Dictionary<AnimatorResolverHelper, System.Action<GameObject>> ();
			m_backwardCallbackMap = new Dictionary<AnimatorResolverHelper, System.Action<GameObject>> ();
		}

		public bool IsAnimating () {
			var isAnimating = false;
			var toBeDeleted = new List<AnimatorResolverHelper> ();

			if (m_forwardCallbackMap.Count > 0) {
				foreach (var kvp in m_forwardCallbackMap) {
					var helper = kvp.Key;
					if (!helper.isAnimating) {
						toBeDeleted.Add (helper);
					} else {
						isAnimating = true;
					}
				}
			}

			for (int i = 0; i < toBeDeleted.Count; i++) {
				var helper = toBeDeleted[i];
				var obj = helper.gameObject;
				var callback = m_forwardCallbackMap[helper];

				m_forwardCallbackMap.Remove (helper);

				PerformForwardCallback (obj, callback);
			}
			toBeDeleted.Clear ();

			if (m_backwardCallbackMap.Count > 0) {
				foreach (var kvp in m_backwardCallbackMap) {
					var helper = kvp.Key;
					if (!helper.isAnimating) {
						toBeDeleted.Add (helper);
					} else {
						isAnimating = true;
					}
				}
			}

			for (int i = 0; i < toBeDeleted.Count; i++) {
				var helper = toBeDeleted[i];
				var obj = helper.gameObject;
				var callback = m_backwardCallbackMap[helper];
				
				m_backwardCallbackMap.Remove (helper);
				
				PerformBackwardCallback (obj, callback);
			}

			return isAnimating;
		}

		public void TransIn (GameObject obj, System.Action<GameObject> callback = null) {
			obj.SetActive (true);

			var helper = obj.GetComponent<AnimatorResolverHelper> ();

			if (helper == null) {
				PerformForwardCallback (obj, callback);
			} else {
				helper.TransIn ();

				if (m_forwardCallbackMap.ContainsKey (helper))
					m_forwardCallbackMap[helper] = callback;
				else
					m_forwardCallbackMap.Add (helper, callback);
			}
		}

		public void TransOut (GameObject obj, System.Action<GameObject> callback = null) {
			var helper = obj.GetComponent<AnimatorResolverHelper> ();

			if (helper == null) {
				PerformBackwardCallback (obj, callback);
			} else {
				helper.TransOut ();

				if (m_backwardCallbackMap.ContainsKey (helper))
					m_backwardCallbackMap[helper] = callback;
				else
					m_backwardCallbackMap.Add (helper, callback);
			}
		}
	}

}