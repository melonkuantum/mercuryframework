﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using Mercury.Helper;
using Mercury.Util;

namespace Mercury {
	
	public class MercuryScene : SingletonComponent<MercuryScene> {
		private static bool s_isAlive = true;
		public static MercuryScene Instance { get { return s_isAlive ? (MercuryScene)_Instance : null; } }

		public static event System.Action<Scene> onSceneChanged;

		public static void ChangeScene (string sceneName, System.Func<object[], IEnumerator> callback, params object[] parameter) {
			var scene = SceneManager.GetActiveScene ();

			if (scene.Equals (sceneName) || string.IsNullOrEmpty (sceneName)) {
				Instance.StartCoroutine (Instance.InvokeCallback (callback, parameter));
			} else {
				Instance.StartCoroutine (Instance.ChangeSceneCo (sceneName, callback, parameter));
			}
		}

		void Start () {
			SceneManager.sceneLoaded += OnSceneLoaded;
		}

		void OnDestroy () {
			SceneManager.sceneLoaded -= OnSceneLoaded;
			s_isAlive = false;
		}

		void OnSceneLoaded (Scene scene, LoadSceneMode mode) {
			if (onSceneChanged != null)
				onSceneChanged (scene);
		}

		IEnumerator ChangeSceneCo (string sceneName, System.Func<object[], IEnumerator> callback, params object[] parameter) {
			var loadOp = SceneManager.LoadSceneAsync (sceneName);
			yield return loadOp;

			if (callback != null)
				yield return StartCoroutine (callback (parameter));

			var unloadOp = ResourceManager.UnloadUnusedAssets ();
			yield return unloadOp;

			System.GC.Collect ();
		}

		IEnumerator InvokeCallback (System.Func<object[], IEnumerator> callback, params object[] parameter) {
			if (callback != null)
				yield return StartCoroutine (callback (parameter));
			else
				yield return null;
		}
	}

}