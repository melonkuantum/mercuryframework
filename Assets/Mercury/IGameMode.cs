using UnityEngine;
using System.Collections;

namespace Mercury {

	public enum GameState { Run, Pause, GameOver };
	public enum WinState { None, Win, Lose };

	public interface IGameMode {
		event System.Action onStart;
		event System.Action onPause;
		event System.Action onResume;
		event System.Action<WinState> onEnd;

		GameState GameState { get; }
		WinState WinState { get; }
		float CompletionRatio { get; }
		string CompletionString { get; }
		string SceneName { get; }
		bool IsPausable { get; set; }
		bool AutoPause { get; set; }

		void PreLoad ();
		void StartGame (params object[] parameter);
		void PauseGame ();
		void ResumeGame ();
		void EndGame ();
	}

}