﻿using UnityEngine;
using System.Collections;

namespace Mercury.Social {

	public delegate void ShareCallback (bool status);

	public interface ISharing {

		void Share (string message, Texture2D img, ShareCallback callback);

	}

}