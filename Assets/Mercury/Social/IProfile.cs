﻿using UnityEngine;
using System.Collections;

namespace Mercury.Social {
	
	public interface IProfile {

		string Id { get; }
		string Name { get; }
		string PhotoUrl { get; }

	}

}