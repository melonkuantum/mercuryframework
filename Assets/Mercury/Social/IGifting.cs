﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Mercury.Social {
	
	public delegate void PostGiftCallback (bool status, string message);
	public delegate void GetGiftCallback<T> (List<T> data);

	public struct ReceivedGift {
		public string giftId;
		public string objectId;
		public string senderName;
		public string senderId;
	}

	public struct RequestedGift {
		public string requestId;
		public string objectId;
		public string requesterName;
		public string requesterId;
	}

	public interface IGifting {

		bool IsSendingGift { get; }
		bool IsAskingGift { get; }
		bool IsDeletingGift { get; }
		bool IsDeletingRequest { get; }
		bool IsFetchingGift { get; }
		bool IsFetchingRequest { get; }

		void SendGift (string title, string message, string objectId, PostGiftCallback callback, params string[] to);
		void AskGift (string title, string message, string objectId, PostGiftCallback callback, params string[] to);
		void DeleteGift (ReceivedGift gift, PostGiftCallback callback);
		void DeleteRequest (RequestedGift request, PostGiftCallback callback);
		void FetchReceivedGift (string objectId, GetGiftCallback<ReceivedGift> callback);
		void FetchRequestedGift (string objectId, GetGiftCallback<RequestedGift> callback);

	}

}