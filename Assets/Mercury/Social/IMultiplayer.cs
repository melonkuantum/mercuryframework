﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Mercury.Social {
	
	public interface IMultiplayer {

		void FetchLeaderboard (string leaderboardId, Leaderboard.LeaderboardType type, System.Action<Leaderboard> callback);
		void SubmitScore (string leaderboardId, float score, System.Action<bool> callback);
		void FetchMapPosition (string id, System.Action<MapPosition> callback);
		void UpdatePosition (string id, int position, System.Action<bool> callback);

	}

	public class Leaderboard {

		public enum LeaderboardType { Global, Friend }

		public class LeaderboardEntry {
			public IProfile player;
			public int position;
			public float score;
		}

		public string id;
		public LeaderboardType type;
		public List<LeaderboardEntry> entries;

	}

	public class MapPosition {

		public class MapPositionEntry {
			public IProfile player;
			public int position;
		}

		public string id;
		public List<MapPositionEntry> entries;

	}

}