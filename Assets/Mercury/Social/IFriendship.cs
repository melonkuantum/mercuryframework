﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Mercury.Social {
	
	public delegate void FetchFriendsCallback (Dictionary<string, IProfile> friends);
	public delegate void InviteFriendsCallback (bool status);

	public interface IFriendship {

		bool IsFetchingFriends { get; }
		bool IsFetchingInvitableFriends { get; }
		Dictionary<string, IProfile> CachedFriends { get; }
		Dictionary<string, IProfile> CachedInvitableFriends { get; }

		void FetchFriends (FetchFriendsCallback callback);
		void FetchInvitableFriends (FetchFriendsCallback callback);
		void FetchAll ();
		void InviteFriends (string title, string message, InviteFriendsCallback callback, params string[] friendIds);

	}

}