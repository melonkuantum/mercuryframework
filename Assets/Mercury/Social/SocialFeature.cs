﻿using UnityEngine;
using System.Collections;

namespace Mercury.Social {
	
	public abstract class SocialFeature {

		public static SocialFeature Active { get; protected set; }

		public static bool CanAutoLogin {
			get {
				if (Active != null) {
					return Active.Authentication.CanAutoLogin;
				}

				return false;
			}
		}

		public static bool IsLogin {
			get {
				if (Active != null) {
					return Active.Authentication.IsLogin;
				}

				return false;
			}
		}

		public static IProfile Profile {
			get {
				if (Active != null) {
					return Active.Authentication.Profile;
				}

				return null;
			}
		}

		public static void AutoLogin (LoginCallback onComplete) {
			if (Active != null) {
				if (Active.Authentication.CanAutoLogin) {
					Active.Authentication.AutoLogin (onComplete);
				} else if (onComplete != null) {
					onComplete (Active.Authentication.Profile);
				}
			}
		}

		public static void Login (LoginCallback onComplete) {
			if (Active != null) {
				if (!Active.Authentication.IsLogin) {
					Active.Authentication.Login (onComplete);
				} else if (onComplete != null) {
					onComplete (Active.Authentication.Profile);
				}
			}
		}

		public static void Logout (System.Action<bool> onComplete) {
			if (Active != null) {
				if (Active.Authentication.IsLogin) {
					Active.Authentication.Logout (onComplete);
				} else if (onComplete != null) {
					onComplete (false);
				}
			}
		}

		public static void Share (string message, Texture2D img, ShareCallback callback) {
			if (Active != null) {
				Active.Sharing.Share (message, img, callback);
			}
		}

		public static IProfile GetFriend (string id) {
			if (Active != null) {
				if (Active.Friendship.CachedFriends != null &&
					!Active.Friendship.IsFetchingFriends) {
					IProfile friend = null;
					Active.Friendship.CachedFriends.TryGetValue (id, out friend);

					return friend;
				}
			}

			return null;
		}

		public static IProfile GetInvitableFriend (string id) {
			if (Active != null) {
				if (Active.Friendship.CachedInvitableFriends != null &&
					!Active.Friendship.IsFetchingInvitableFriends) {
					IProfile friend = null;
					Active.Friendship.CachedInvitableFriends.TryGetValue (id, out friend);

					return friend;
				}
			}

			return null;
		}

		public static void FetchFriends (FetchFriendsCallback callback, bool forceFetch = false) {
			if (callback != null && Active != null) {
				if (Active.Friendship.CachedFriends != null &&
					Active.Friendship.CachedFriends.Count > 0 &&
					!Active.Friendship.IsFetchingFriends && !forceFetch)
					callback (Active.Friendship.CachedFriends);
				else
					Active.Friendship.FetchFriends (callback);
			}
		}

		public static void FetchInvitableFriends (FetchFriendsCallback callback, bool forceFetch = false) {
			if (callback != null && Active != null) {
				if (Active.Friendship.CachedInvitableFriends != null &&
					Active.Friendship.CachedInvitableFriends.Count > 0 &&
					!Active.Friendship.IsFetchingInvitableFriends && !forceFetch)
					callback (Active.Friendship.CachedInvitableFriends);
				else
					Active.Friendship.FetchInvitableFriends (callback);
			}
		}

		public static void InviteFriends (string title, string message, InviteFriendsCallback callback, params string[] friendIds) {
			if (Active != null) {
				if (friendIds != null && friendIds.Length > 0)
					Active.Friendship.InviteFriends (title, message, callback, friendIds);
				else
					Active.Friendship.InviteFriends (title, message, callback);
			}
		}

		public static void SendGift (string title, string message, string objectId, PostGiftCallback callback, params string[] to) {
			if (Active != null) {
				if (to != null && to.Length > 0)
					Active.Gifting.SendGift (title, message, objectId, callback, to);
				else
					Active.Gifting.SendGift (title, message, objectId, callback);
			}
		}

		public static void AskGift (string title, string message, string objectId, PostGiftCallback callback, params string[] to) {
			if (Active != null) {
				if (to != null && to.Length > 0)
					Active.Gifting.AskGift (title, message, objectId, callback, to);
				else
					Active.Gifting.AskGift (title, message, objectId, callback);
			}
		}

		public static void DeleteGift (ReceivedGift gift, PostGiftCallback callback) {
			if (Active != null) {
				Active.Gifting.DeleteGift (gift, callback);
			}
		}

		public static void DeleteRequest (RequestedGift request, PostGiftCallback callback) {
			if (Active != null) {
				Active.Gifting.DeleteRequest (request, callback);
			}
		}

		public static void FetchReceivedGift (string objectId, GetGiftCallback<ReceivedGift> callback) {
			if (Active != null)
				Active.Gifting.FetchReceivedGift (objectId, callback);
		}

		public static void FetchRequestedGift (string objectId, GetGiftCallback<RequestedGift> callback) {
			if (Active != null)
				Active.Gifting.FetchRequestedGift (objectId, callback);
		}

		public static void FetchLeaderboard (string leaderboardId, Leaderboard.LeaderboardType type, System.Action<Leaderboard> callback) {
			if (Active != null) {
				Active.Multiplayer.FetchLeaderboard (leaderboardId, type, callback);
			}
		}

		public static void SubmitScore (string leaderboardId, float score, System.Action<bool> callback) {
			if (Active != null) {
				Active.Multiplayer.SubmitScore (leaderboardId, score, callback);
			}
		}

		public static void FetchMapPosition (string id, System.Action<MapPosition> callback) {
			if (Active != null) {
				Active.Multiplayer.FetchMapPosition (id, callback);
			}
		}

		public static void UpdatePosition (string id, int position, System.Action<bool> callback) {
			if (Active != null) {
				Active.Multiplayer.UpdatePosition (id, position, callback);
			}
		}

		public abstract IAuthentication Authentication { get; }
		public abstract ISharing Sharing { get; }
		public abstract IFriendship Friendship { get; }
		public abstract IGifting Gifting { get; }
		public abstract IMultiplayer Multiplayer { get; }

		public abstract void Init (System.Action<bool> onComplete);

	}

}