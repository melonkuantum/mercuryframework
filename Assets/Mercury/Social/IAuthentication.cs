﻿using UnityEngine;
using System.Collections;

namespace Mercury.Social {

	public delegate void LoginCallback (IProfile profile);

	public interface IAuthentication {

		event LoginCallback onLoggedIn;
		event System.Action<bool> onLoggedOut;

		bool CanAutoLogin { get; }
		bool IsLogin { get; }
		IProfile Profile { get; }
		void AutoLogin (LoginCallback onComplete);
		void Login (LoginCallback onComplete);
		void Logout (System.Action<bool> onComplete);

	}

}