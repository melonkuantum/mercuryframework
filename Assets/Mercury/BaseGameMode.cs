using UnityEngine;
using System.Collections;

namespace Mercury {

	public abstract class BaseGameMode : ScriptableObject, IGameMode {
		public event System.Action onStart;
		public event System.Action onPause;
		public event System.Action onResume;
		public event System.Action<WinState> onEnd;
		
		public GameState GameState { get; protected set; }
		public WinState WinState { get; protected set; }
		public abstract float CompletionRatio { get; }
		public abstract string CompletionString { get; }
		public abstract string SceneName { get; }
		public bool IsPausable { get; set; }
		public bool AutoPause { get; set; }

		protected void InvokeOnStart () {
			if (onStart != null)
				onStart ();
		}
		
		protected void InvokeOnPause () {
			if (onPause != null)
				onPause ();
		}
		
		protected void InvokeOnResume () {
			if (onResume != null)
				onResume ();
		}
		
		protected void InvokeOnEnd () {
			if (onEnd != null)
				onEnd (WinState);
		}

		public virtual void PreLoad () {
			GameState = GameState.GameOver;
			WinState = WinState.None;
			IsPausable = true;
			AutoPause = true;
		}

		public abstract void StartGame (params object[] parameter);
		public abstract void PauseGame ();
		public abstract void ResumeGame ();
		public abstract void EndGame ();
	}

}