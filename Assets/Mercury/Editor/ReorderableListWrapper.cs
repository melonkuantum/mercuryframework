﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Collections;
using System.Collections.Generic;

public class ReorderableListWrapper {

	public const float REORDERABLE_HEADER_HEIGHT = 17;
	public const float REORDERABLE_FOOTER_HEIGHT = 20;
	public const float REORDERABLE_EMPTY_FOOTER_HEIGHT = 4;
	public const float REORDERABLE_ELEMENT_HEIGHT = 22;
	public const float REORDERABLE_ELEMENT_PADDING = 4;
	public const float EMPTY_REORDERABLE_HEIGHT = 22;

	public delegate void CallbackGenerationDelegate (ReorderableList roList);

	public CallbackGenerationDelegate generateHeaderCallback;
	public CallbackGenerationDelegate generateElementCallback;
	public CallbackGenerationDelegate generateElementHeightCallback;

	Dictionary<string, ReorderableList> m_roListMap = new Dictionary<string, ReorderableList> ();
	bool m_useFullPath, m_isEditable;

	protected string GetPropertyPath (SerializedProperty property) {
		return m_useFullPath ? property.propertyPath : property.name;
	}

	protected virtual ReorderableList GenerateNewList (SerializedProperty property) {
		var roList = new ReorderableList(property.serializedObject, property, true, true, m_isEditable, m_isEditable);

		generateHeaderCallback (roList);
		generateElementCallback (roList);
		generateElementHeightCallback (roList);

		return roList;
	}

	public ReorderableList GetReorderableList (SerializedProperty property) {
		ReorderableList roList = null;

		if (!m_roListMap.TryGetValue (GetPropertyPath (property), out roList)) {
			roList = GenerateNewList (property);
			m_roListMap.Add (GetPropertyPath (property), roList);
		}

		return roList;
	}

	public void DefaultGenerator () {
		generateHeaderCallback = (roList) => {
			roList.drawHeaderCallback = (Rect rect) => {  
				EditorGUI.LabelField(rect, roList.serializedProperty.displayName);
			};
		};

		generateElementCallback = (roList) => {
			roList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
				var element = roList.serializedProperty.GetArrayElementAtIndex (index);
				rect.y += 2;
				EditorGUI.PropertyField (rect, element, true);
			};
		};

		generateElementHeightCallback = (roList) => {
			roList.elementHeightCallback = ((index) => {
				var p = roList.serializedProperty.GetArrayElementAtIndex (index);
				float height = GetHeight (p, new GUIContent (p.displayName, p.tooltip), m_isEditable) + REORDERABLE_ELEMENT_PADDING;

				return height;
			});
		};
	}

	public ReorderableListWrapper (bool useFullPath, bool isEditable) {
		m_useFullPath = useFullPath;
		m_isEditable = isEditable;
		DefaultGenerator ();
	}

	public ReorderableListWrapper () : this (true, true) {
	}

	public static float GetHeight (SerializedProperty property, GUIContent label, bool withFooter = true) {
		if (property.isArray && property.propertyType != SerializedPropertyType.String) {
			var height = REORDERABLE_HEADER_HEIGHT;

			if (property.arraySize == 0) {
				height += EMPTY_REORDERABLE_HEIGHT;
			} else {
				for (int i = 0; i < property.arraySize; i++) {
					var e = property.GetArrayElementAtIndex (i);
					height += GetHeight (e, new GUIContent (e.displayName, e.tooltip)) + REORDERABLE_ELEMENT_PADDING;
				}
			}

			height += withFooter ? REORDERABLE_FOOTER_HEIGHT : REORDERABLE_EMPTY_FOOTER_HEIGHT;

			return height;
		} else {
			return EditorGUI.GetPropertyHeight (property, label, true);
		}
	}

}
