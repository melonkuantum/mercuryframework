using UnityEngine;
using UnityEditor;
using System.Collections;

using Mercury;

[CustomEditor (typeof (MercuryGame), true)]
public class MercuryGameEditor : Editor {
	bool m_confirm;

	public override void OnInspectorGUI () {
		base.OnInspectorGUI ();

		if (m_confirm) {
			GUI.backgroundColor = Color.green;
			if (GUILayout.Button ("Cancel"))
				m_confirm = false;

			GUI.backgroundColor = Color.red;
			if (GUILayout.Button ("Delete")) {
				var t = (MercuryGame)target;
				t.ResetData ();
				m_confirm = false;
			}

			GUI.backgroundColor = Color.white;
		} else {
			if (GUILayout.Button ("Reset"))
			    m_confirm = true;
		}
	}
}