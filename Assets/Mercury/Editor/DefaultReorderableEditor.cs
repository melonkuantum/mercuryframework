﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Collections;

[CustomEditor (typeof (Object), true), CanEditMultipleObjects]
public class DefaultReorderableEditor : Editor {
	
	protected ReorderableListWrapper m_roListMap;

	protected virtual ReorderableListWrapper GetReorderableListWrapper () {
		if (m_roListMap == null)
			m_roListMap = new ReorderableListWrapper (false, true);

		return m_roListMap;
	}

	protected virtual void OnEnable () {
		GetReorderableListWrapper ();
	}

	protected virtual void OnDisable () {
		m_roListMap = null;
	}

//	ReorderableList GetReorderableList (SerializedProperty property) {
//		ReorderableList roList = null;
//
//		if (!m_roListMap.TryGetValue (property.name, out roList)) {
//			roList = new ReorderableList(serializedObject, property, true, true, true, true);
//
//			roList.drawHeaderCallback = (Rect rect) => {  
//				EditorGUI.LabelField(rect, property.displayName);
//			};
//			
//			roList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
//				var element = roList.serializedProperty.GetArrayElementAtIndex (index);
//				EditorGUI.PropertyField (rect, element, true);
//			};
//
//			roList.elementHeightCallback = ((index) => {
//				Repaint ();
//
//				var p = roList.serializedProperty.GetArrayElementAtIndex (index);
//				float height = EditorGUI.GetPropertyHeight (p, new GUIContent (p.displayName, p.tooltip), true);
//
//				return height;
//			});
//
//			m_roListMap.Add (property.name, roList);
//		}
//
//		return roList;
//	}

	public override void OnInspectorGUI() {
		serializedObject.Update();

		var iter = serializedObject.GetIterator ();
		if (iter.NextVisible (true)) {
			do {
				if (iter.isArray && iter.propertyType != SerializedPropertyType.String) {
					var roList = GetReorderableListWrapper ().GetReorderableList (iter.Copy ());
					roList.DoLayoutList ();
				} else {
					EditorGUILayout.PropertyField (iter.Copy (), true);
				}
			} while (iter.NextVisible (false));
		}

		serializedObject.ApplyModifiedProperties();
	}

}
