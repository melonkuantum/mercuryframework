﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Collections;

[CustomPropertyDrawer (typeof (object), true)]
public class DefaultReorderableDrawer : PropertyDrawer {

	const float INDENT_PIX = 5;

	protected ReorderableListWrapper m_roListMap;

	protected virtual ReorderableListWrapper GetReorderableListWrapper () {
		if (m_roListMap == null)
			m_roListMap = new ReorderableListWrapper (true, true);

		return m_roListMap;
	}

	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
		if (property.hasVisibleChildren) {
			position.y += 1;
			EditorGUI.LabelField (position, label);
			position = new Rect (position.x+INDENT_PIX, position.y+EditorGUIUtility.singleLineHeight + 2, position.width-INDENT_PIX, position.height);

			var iter = property.Copy ();
			var depth = iter.depth;
			iter.NextVisible (true);
			do {
				var child = iter.Copy ();

				if (child.isArray && child.propertyType != SerializedPropertyType.String) {
					var roList = GetReorderableListWrapper ().GetReorderableList (child.Copy ());
					roList.DoList (position);
				} else {
					EditorGUI.PropertyField (position, child, true);
				}

				position.y += ReorderableListWrapper.GetHeight (child, new GUIContent (child.displayName, child.tooltip)) + 2;
			} while (iter.NextVisible (false) && iter.depth > depth);
		} else {
			EditorGUI.PropertyField (position, property, label, true);
		}
	}

	public override float GetPropertyHeight (SerializedProperty property, GUIContent label) {
		if (property.hasVisibleChildren) {
			var height = EditorGUIUtility.singleLineHeight + 1;

			var iter = property.Copy ();
			var depth = iter.depth;
			iter.NextVisible (true);
			do {
				var child = iter.Copy ();
				height += ReorderableListWrapper.GetHeight (child, new GUIContent (child.displayName, child.tooltip)) + 2;
			} while (iter.NextVisible (false) && iter.depth > depth);

			return height;
		} else {
			return base.GetPropertyHeight (property, label);
		}
	}

}
