﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

using Mercury.Util;

public class ResourceManagerViewer : EditorWindow {
	Dictionary<string, Object> m_loadedAssets;

	[MenuItem ("Window/Mercury/Resource Manager Viewer")]
	static void Init () {
		var window = EditorWindow.GetWindow<ResourceManagerViewer> ("Resource Manager Viewer");
		window.Show ();
	}

	void OnGUI () {
		if (Application.isPlaying) {
			if (m_loadedAssets == null) {
				var t = typeof(ResourceManager);
				var la = t.GetField ("s_loadedAssets", BindingFlags.NonPublic | BindingFlags.Static);
				m_loadedAssets = la.GetValue (null) as Dictionary<string, Object>;
			}

			if (m_loadedAssets != null && m_loadedAssets.Count > 0) {
				foreach (var asset in m_loadedAssets.Values)
					EditorGUILayout.LabelField (asset.name + " : " + asset.GetType ().ToString ());
			}
		}
	}
}
