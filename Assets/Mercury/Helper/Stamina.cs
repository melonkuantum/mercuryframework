﻿using UnityEngine;
using System.Collections;

using Mercury.Helper.StatSystem;
using Mercury.Util;

namespace Mercury.Helper {

	[System.Serializable]
	public class Stamina {
		[SerializeField]
		BaseStat m_maxStamina;
		public int MaxStamina { get { return (int)m_maxStamina.Value; } }

		[SerializeField]
		int m_stamina;
		public int CurrentStamina {
			get {
				CalculateDifference ();
				return m_stamina;
			}
		}

		[SerializeField]
		float m_regenTime;
		public float RegenTime { get { return m_regenTime; } }

		[SerializeField]
		double m_lastAdded;
		public double LastAdded { get { return m_lastAdded; } }

		public float TimeUntilRegen {
			get {
				var nextRegen = m_lastAdded + m_regenTime;
				var current = TimeUtil.CurrentUnix;

				return Mathf.Max ((float)(nextRegen - current), 0);
			}
		}

		public float TimeUntilFull {
			get {
				var fullAt = m_lastAdded + (m_regenTime * (MaxStamina - m_stamina));
				var current = TimeUtil.CurrentUnix;

				return Mathf.Max ((float)(fullAt - current), 0);
			}
		}

		public int amountPerRegen = 1;

		public Stamina (int maxStamina, int stamina, float regenTime, double lastAdded) {
			m_maxStamina = new BaseStat (maxStamina);
			m_stamina = stamina;
			m_regenTime = regenTime;
			m_lastAdded = lastAdded;

			CalculateDifference ();
		}

		public Stamina (int maxStamina, float regenTime) : this (maxStamina, maxStamina, regenTime, TimeUtil.CurrentUnix) {
		}

		void CalculateDifference () {
			if (m_stamina < MaxStamina) {
				var current = TimeUtil.CurrentUnix;
				var delta = current - m_lastAdded;
				if (delta >= m_regenTime) {
					var regenCount = Mathf.Min (Mathf.FloorToInt ((float)(delta / m_regenTime)), MaxStamina - m_stamina);
					m_stamina += regenCount * amountPerRegen;
					m_lastAdded += regenCount * m_regenTime;
				}
			}
		}

		public bool Use (int amount = 1) {
			if (amount > 0 &&
			    amount <= CurrentStamina) {
				var fromMax = m_stamina >= MaxStamina;

				m_stamina -= amount;

				if (m_stamina < MaxStamina && fromMax)
					m_lastAdded = TimeUtil.CurrentUnix;

				return true;
			}

			return false;
		}

		public void ForceAdd (int amount = 1) {
			m_stamina += amount;
		}

		public void ModifyMaxStamina (params BaseModifier[] modifiers) {
			if (modifiers != null && modifiers.Length > 0) {
				for (int i = 0; i < modifiers.Length; i++) {
					m_maxStamina.AddModifier (modifiers[i]);
				}
			}
		}

		public void ResetMaxStamina () {
			m_maxStamina.ClearModifiers ();
		}

	}

}