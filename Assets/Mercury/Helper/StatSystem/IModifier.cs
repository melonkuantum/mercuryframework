
namespace Mercury.Helper.StatSystem {

	public interface IModifier {
		float Amount { get; }
		int Priority { get; }

		float Modify (float baseValue);
	}

}