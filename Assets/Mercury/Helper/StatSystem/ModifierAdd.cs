
namespace Mercury.Helper.StatSystem {

	public class ModifierAdd : BaseModifier {

		public ModifierAdd (float amount, int priority) : base (amount, priority) {
		}

		public override float Modify (float baseValue) {
			return baseValue + Amount;
		}
	}

}