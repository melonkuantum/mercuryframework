﻿using UnityEngine;
using System.Collections.Generic;

namespace Mercury.Helper.StatSystem {

	public abstract class ModifierGroup : BaseModifier {
		[SerializeField]
		protected List<IModifier> m_modifiers;

		public int Count { get { return m_modifiers.Count; } }

		public ModifierGroup (float amount, int priority) : base (amount, priority) {
			m_modifiers = new List<IModifier> ();
		}

		public ModifierGroup (float amount, int priority, params IModifier[] modifiers) :
		this (amount, priority) {
			foreach (var modifier in modifiers)
				AddModifier (modifier);
		}

		public virtual bool AddModifier (IModifier modifier) {
			m_modifiers.Add (modifier);
			return true;
		}

		public virtual bool RemoveModifier (IModifier modifier) {
			return m_modifiers.Remove (modifier);
		}

		public virtual bool ClearModifiers () {
			m_modifiers.Clear ();
			return true;
		}
	}

}