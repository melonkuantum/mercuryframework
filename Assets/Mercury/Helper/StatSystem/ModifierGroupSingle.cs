using UnityEngine;
using System.Collections;

namespace Mercury.Helper.StatSystem {

	public class ModifierGroupSingle : ModifierGroup {
		public enum Mode { First, Last, Custom }

		[SerializeField]
		Mode m_mode;
		[SerializeField]
		int m_index;

		public ModifierGroupSingle (float amount, int priority, Mode mode = Mode.First, int index = 0) :
		base (amount, priority) {
			m_mode = mode;
			m_index = index;
		}

		public override float Modify (float baseValue) {
			if (m_modifiers.Count == 0)
				return baseValue;

			switch (m_mode) {
			case Mode.First:
				return m_modifiers[0].Modify (baseValue);
			case Mode.Last:
				return m_modifiers[m_modifiers.Count-1].Modify (baseValue);
			case Mode.Custom:
				if (m_index < 0)
					return m_modifiers[0].Modify (baseValue);
				else if (m_index > m_modifiers.Count)
					return m_modifiers[m_modifiers.Count-1].Modify (baseValue);
				else
					return m_modifiers[m_index].Modify (baseValue);
			default:
				return baseValue;
			}
		}
	}

}