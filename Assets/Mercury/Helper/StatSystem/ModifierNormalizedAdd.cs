
namespace Mercury.Helper.StatSystem {

	public class ModifierNormalizedAdd : BaseModifier {

		public ModifierNormalizedAdd (float amount, int priority) : base (amount, priority) {
		}

		public override float Modify (float baseValue) {
			return 1 - ((1 - baseValue) * (1 - Amount));
		}
	}

}