
namespace Mercury.Helper.StatSystem {

	public class ModifierGroupAddAll : ModifierGroup {
		public ModifierGroupAddAll (float amount, int priority) : base (amount, priority) {
		}
		
		public ModifierGroupAddAll (float amount, int priority, params IModifier[] modifiers) :
		base (amount, priority, modifiers) {
		}

		public override float Modify (float baseValue) {
			float temp = baseValue;
			foreach (var modifier in m_modifiers)
				temp += modifier.Modify (baseValue);

			return temp;
		}
	}

}