﻿using UnityEngine;

namespace Mercury.Helper.StatSystem {

	[System.Serializable]
	public class BaseStat : IModifiable {
		public ModifierGroup modifiers;

		[SerializeField]
		float m_baseValue;
		public float BaseValue {
			get {
				return m_baseValue;
			}
			set {
				m_isDirty = true;
				m_baseValue = value;
			}
		}

		[SerializeField]
		float m_value;
		public float Value {
			get {
				if (m_isDirty) {
					m_value = modifiers.Modify (m_baseValue);
					m_isDirty = false;
				}

				return m_value;
			}
		}

		bool m_isDirty;

		public BaseStat (float baseValue, ModifierGroup modifiers) {
			this.modifiers = modifiers;
			m_baseValue = baseValue;
			m_isDirty = true;
		}

		public BaseStat (float baseValue) : this (baseValue, new ModifierSelector (0, 0)) {
		}

		public BaseStat (float baseValue, params IModifier[] modifiers) : this (baseValue) {
			foreach (var modifier in modifiers)
				AddModifier (modifier);
		}

		public bool AddModifier (IModifier modifier) {
			if (modifiers.AddModifier (modifier)) {
				m_isDirty = true;
				return true;
			}

			return false;
		}

		public bool RemoveModifier (IModifier modifier) {
			if (modifiers.RemoveModifier (modifier)) {
				m_isDirty = true;
				return true;
			}

			return false;
		}

		public bool ClearModifiers () {
			if (modifiers.ClearModifiers ()) {
				m_isDirty = true;
				return true;
			}

			return false;
		}
	}

}