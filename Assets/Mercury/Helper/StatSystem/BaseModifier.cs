﻿
namespace Mercury.Helper.StatSystem {

	[System.Serializable]
	public abstract class BaseModifier : IModifier {
		public float Amount { get; protected set; }
		public int Priority { get; protected set; }

		public BaseModifier (float amount, int priority) {
			Amount = amount;
			Priority = priority;
		}

		public abstract float Modify (float baseValue);
	}

}