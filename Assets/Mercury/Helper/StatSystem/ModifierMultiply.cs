
namespace Mercury.Helper.StatSystem {

	public class ModifierMultiply : BaseModifier {

		public ModifierMultiply (float amount, int priority) : base (amount, priority) {
		}

		public override float Modify (float baseValue) {
			return baseValue * Amount;
		}
	}

}