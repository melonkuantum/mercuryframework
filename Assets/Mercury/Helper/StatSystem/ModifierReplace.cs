
namespace Mercury.Helper.StatSystem {

	public class ModifierReplace : BaseModifier {

		public ModifierReplace (float amount, int priority) : base (amount, priority) {
		}

		public override float Modify (float baseValue) {
			return Amount;
		}
	}

}