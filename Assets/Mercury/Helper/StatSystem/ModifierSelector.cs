using UnityEngine;
using System.Collections.Generic;

namespace Mercury.Helper.StatSystem {

	public class ModifierSelector : ModifierGroupSequential {
		[SerializeField]
		ModifierGroup m_replaceGroup;
		public ModifierGroup ReplaceGroup {
			get {
				m_isDirty = true;

				return m_replaceGroup;
			}
		}

		[SerializeField]
		ModifierGroup m_normalizedAddGroup;
		public ModifierGroup NormalizedAddGroup {
			get {
				m_isDirty = true;
				
				return m_normalizedAddGroup;
			}
		}

		[SerializeField]
		ModifierGroup m_addGroup;
		public ModifierGroup AddGroup {
			get {
				m_isDirty = true;
				
				return m_addGroup;
			}
		}

		[SerializeField]
		ModifierGroup m_multiplyGroup;
		public ModifierGroup MultiplyGroup {
			get {
				m_isDirty = true;
				
				return m_multiplyGroup;
			}
		}

		bool m_isDirty;

		void Sort () {
			m_modifiers.Sort (delegate(IModifier x, IModifier y) {
				if (x.Priority < y.Priority)
					return -1;
				else if (x.Priority > y.Priority)
					return 1;
				else
					return 0;
			});

			m_isDirty = false;
		}

		public ModifierSelector (float amount, int priority) : base (amount, priority) {
			m_replaceGroup = new ModifierGroupSingle (0, 0, ModifierGroupSingle.Mode.Last);
			m_normalizedAddGroup = new ModifierGroupSequential (0, 1);
			m_addGroup = new ModifierGroupSequential (0, 2);
			m_multiplyGroup = new ModifierGroupAddAll (0, 3);

			m_modifiers.Add (m_replaceGroup);
			m_modifiers.Add (m_normalizedAddGroup);
			m_modifiers.Add (m_addGroup);
			m_modifiers.Add (m_multiplyGroup);

			Sort ();
		}
		
		public ModifierSelector (float amount, int priority, params IModifier[] modifiers) :
		this (amount, priority) {
			foreach (var modifier in modifiers)
				AddModifier (modifier);
		}

		public override bool AddModifier (IModifier modifier) {
			if (modifier is ModifierReplace) {
				m_replaceGroup.AddModifier (modifier);
				return true;
			} else if (modifier is ModifierNormalizedAdd) {
				m_normalizedAddGroup.AddModifier (modifier);
				return true;
			} else if (modifier is ModifierAdd) {
				m_addGroup.AddModifier (modifier);
				return true;
			} else if (modifier is ModifierMultiply) {
				m_multiplyGroup.AddModifier (modifier);
				return true;
			}

			return false;
		}
		
		public override bool RemoveModifier (IModifier modifier) {
			if (modifier is ModifierReplace)
				return m_replaceGroup.RemoveModifier (modifier);
			else if (modifier is ModifierNormalizedAdd)
				return m_normalizedAddGroup.RemoveModifier (modifier);
			else if (modifier is ModifierAdd)
				return m_addGroup.RemoveModifier (modifier);
			else if (modifier is ModifierMultiply)
				return m_multiplyGroup.RemoveModifier (modifier);

			return false;
		}

		public override bool ClearModifiers () {
			m_replaceGroup.ClearModifiers ();
			m_normalizedAddGroup.ClearModifiers ();
			m_addGroup.ClearModifiers ();
			m_multiplyGroup.ClearModifiers ();

			return true;
		}

		public override float Modify (float baseValue) {
			if (m_isDirty) Sort ();

			return base.Modify (baseValue);
		}
	}

}