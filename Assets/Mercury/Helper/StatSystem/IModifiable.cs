﻿
namespace Mercury.Helper.StatSystem {

	public interface IModifiable {
		float BaseValue { get; }
		float Value { get; }

		bool AddModifier (IModifier modifier);
		bool RemoveModifier (IModifier modifier);
		bool ClearModifiers ();
	}

}
