﻿using UnityEngine;
using System.Collections;

namespace Mercury.Helper {

	public static class SpriteHelper {

		public struct PivotAlignment {

			public static PivotAlignment Center = new PivotAlignment () { x = 0.5f, y = 0.5f };

			public float x, y;
		}

		public static Vector2 GetNormalizedPivot (this Sprite s) {
			return new Vector2 (s.pivot.x / (s.bounds.size.x * s.pixelsPerUnit), s.pivot.y / (s.bounds.size.y * s.pixelsPerUnit));
		}

		public static Vector2 GetWorldUnitPivot (this Sprite s) {
			return new Vector2 (s.pivot.x / s.pixelsPerUnit, s.pivot.y / s.pixelsPerUnit);
		}

		public static Vector2 GetWorldUnitOffset (this Sprite s, PivotAlignment alignment) {
			var size = s.bounds.size;
			return new Vector2 (alignment.x * size.x, alignment.y * size.y);
		}

	}

}