﻿using UnityEngine;
using System.Collections;

namespace Mercury.Helper {

	public class CameraFitter2D : MonoBehaviour {
		static float s_yOffset;
		public static float YOffset { get { return s_yOffset; } }

		public enum StretchMode {Fixed, Stretch, FitWidth, FitHeight};

		public int targetWidth;
		public int targetHeight;
		public int pixelsToUnits;
		public StretchMode stretchMode;

		float m_size = 1;

		void Awake () {
			Fit ();
		}

		public void Fit () {
			float scale = 1;
			var targetSize = (float)targetHeight / (float)(2 * pixelsToUnits);

			switch (stretchMode) {
			case StretchMode.Fixed:
				scale = (float)targetHeight/(float)Screen.height;
				GetComponent<Camera>().rect = new Rect ((float)(Screen.width-targetWidth)/(2*Screen.width),
					(float)(Screen.height-targetHeight)/(2*Screen.height),
					scale, scale);
				m_size = targetSize;
				break;
			case StretchMode.Stretch:
			case StretchMode.FitWidth:
				scale = (float)(targetWidth * Screen.height)/(float)(targetHeight * Screen.width);
				m_size = (float)(scale * targetHeight) / (float)(2 * pixelsToUnits);
				break;
			case StretchMode.FitHeight:
				m_size = targetSize;
				break;
			}

			s_yOffset = m_size - targetSize;

			GetComponent<Camera>().orthographicSize = m_size;
		}
	}

}