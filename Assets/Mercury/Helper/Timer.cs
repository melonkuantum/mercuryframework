﻿using UnityEngine;
using System.Collections;

using Mercury;

namespace Mercury.Helper {

	public class Timer {
		public enum UpdateType { Scaled, Unscaled }
		public enum RunningState { Stop, Run }

		public delegate void TickDelegate (float currentTime, float currentdelta);

		public event System.Action onStart;
		public event System.Action onStop;
		public event System.Action onReset;
		public event TickDelegate onTick;
		public event System.Action onComplete;

		public float duration;
		public bool isLoop;

		UpdateType m_updateType;
		public UpdateType updateType {
			get { return m_updateType; }
			set {
				if (m_updateType != value) {
					m_updateType = value;
					ConfigureUpdateFunction ();
				}
			}
		}

		float m_time;
		public float Time { get { return m_time; } }
		public float NormalizedTime { get { return (duration - m_time) / duration; } }

		RunningState m_state;
		public RunningState State { get { return m_state; } }

		public Timer (float duration, bool isLoop, UpdateType updateType) {
			this.duration = duration;
			this.isLoop = isLoop;
			m_time = duration;
			m_state = RunningState.Stop;
			m_updateType = updateType;
		}

		public Timer (float duration, bool isLoop) : this (duration, isLoop, UpdateType.Scaled) {
		}

		public Timer (float duration) : this (duration, false, UpdateType.Scaled) {
		}

		public Timer () : this (1f, false, UpdateType.Scaled) {
		}

		void Tick (float dt) {
			m_time -= dt;
			var isComplete = m_time <= 0;

			if (isComplete) {
				if (isLoop) {
					m_time = duration - m_time;
				} else {
					_Stop ();
					m_time = 0;
				}
			}

			if (onTick != null)
				onTick (m_time, dt);

			if (isComplete &&
			    onComplete != null)
				onComplete ();
		}

		void ConfigureUpdateFunction () {
			MercuryUpdate.onGameUpdate -= Tick;
			MercuryUpdate.onUnscaledGameUpdate -= Tick;

			if (m_state == RunningState.Run) {
				switch (m_updateType) {
				case UpdateType.Scaled:
					MercuryUpdate.onGameUpdate += Tick;
					break;
				case UpdateType.Unscaled:
					MercuryUpdate.onUnscaledGameUpdate += Tick;
					break;
				}
			}
		}

		void _Stop () {
			m_state = RunningState.Stop;
			ConfigureUpdateFunction ();
		}

		public void Start () {
			if (m_state != RunningState.Run) {
				if (onStart != null)
					onStart ();

				m_state = RunningState.Run;

				ConfigureUpdateFunction ();
			}
		}

		public void Stop () {
			if (m_state != RunningState.Stop) {
				_Stop ();

				if (onStop != null)
					onStop ();
			}
		}

		public void Reset (float factor) {
			m_time = (1-factor) * duration;

			if (onReset != null)
				onReset ();
		}

		public void Reset () {
			Reset (0);
		}
	}

}