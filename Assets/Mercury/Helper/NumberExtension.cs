﻿using UnityEngine;
using System.Collections;

namespace Mercury.Helper {
	
	public static class NumberExtension {

		public static string ToMMSS (this float time) {
			var m = Mathf.FloorToInt (time / 60);
			var s = Mathf.FloorToInt (time % 60);

			return m.ToString ("00") + ":" + s.ToString ("00");
		}

		public static string ToHHMMSS (this float time) {
			var h = Mathf.FloorToInt (time / 3600);
			var mInS = time % 3600;

			return h.ToString ("00") + ":" + mInS.ToMMSS ();
		}

		public static string ToDDHHMMSS (this float time) {
			var d = Mathf.FloorToInt (time / 86400);
			var hInS = time % 86400;

			if (d < 1) {
				return hInS.ToHHMMSS ();
			} else {
				var h = Mathf.FloorToInt (hInS / 3600);

				return d.ToString () + "d " + h.ToString () + "h";
			}
		}

		public static string ToMMSS (this double time) {
			return ((float)time).ToMMSS ();
		}

		public static string ToHHMMSS (this double time) {
			return ((float)time).ToHHMMSS ();
		}

		public static string ToDDHHMMSS (this double time) {
			return ((float)time).ToDDHHMMSS ();
		}

	}

}