﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NamedGameObject : MonoBehaviour {

	public enum OverrideMode { UseOld, UseNew }

	static Dictionary<string, NamedGameObject> s_cache = new Dictionary<string, NamedGameObject> ();

	public static NamedGameObject Get (string key) {
		NamedGameObject obj = null;
		s_cache.TryGetValue (key, out obj);

		return obj;
	}

	public string key;
	public OverrideMode overrideMode;
	bool m_isCached;

	void Awake () {
		Cache ();
	}

	void OnDestroy () {
		Uncache ();
	}

	public void UpdateKey (string newKey) {
		Uncache ();
		key = newKey;
		Cache ();
	}

	public void Cache () {
		if (!m_isCached) {
			if (!s_cache.ContainsKey (key)) {
				s_cache.Add (key, this);
				m_isCached = true;
			} else if (overrideMode == OverrideMode.UseNew) {
				s_cache [key].Uncache ();
				s_cache.Add (key, this);
				m_isCached = true;
			}
		}
	}

	public void Uncache () {
		if (m_isCached) {
			s_cache.Remove (key);
		}

		m_isCached = false;
	}

}
