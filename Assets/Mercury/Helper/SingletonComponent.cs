﻿using UnityEngine;
using System.Collections;

namespace Mercury.Helper {

	public class SingletonComponent<T> : MonoBehaviour where T : MonoBehaviour {

		private static T s_instance;
		protected static T _Instance {
			get {
				if (s_instance == null) {
					var instances = GameObject.FindObjectsOfType<T> ();
					if (instances != null && instances.Length > 0) {
						if (instances.Length == 1) {
							s_instance = instances[0];
						} else {
							Debug.LogError ("You have more than one instance of " + typeof(T).Name);
							s_instance = instances[0];
							for (int i = 1; i < instances.Length; i++)
								Destroy (instances[i].gameObject);
						}
					} else {
						var go = new GameObject (typeof (T).Name, typeof (T));
						s_instance = go.GetComponent<T> ();
					}

					DontDestroyOnLoad (s_instance);
				}

				return s_instance;
			}
		}

	}

}