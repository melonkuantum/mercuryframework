﻿using UnityEngine;
using System.Collections;

namespace Mercury.Pooling {

	public interface IPool<T> where T : class, IPoolable {
		T Obtain ();
		void Store (T poolable);
	}

	public interface IPoolable {
		void Reset ();
		void Release ();
	}

}