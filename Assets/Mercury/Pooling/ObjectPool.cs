﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Mercury.Pooling {

	public sealed class ObjectPool : MonoBehaviour
	{
		static ObjectPool _instance;

		Dictionary<Object, List<Object>> objectLookup = new Dictionary<Object, List<Object>>();
		Dictionary<Object, int> poolCountLookup = new Dictionary<Object, int> ();
		Dictionary<Object, Object> prefabLookup = new Dictionary<Object, Object>();
		
		public static void Clear()
		{
			foreach (var o in instance.prefabLookup.Keys) {
				if (o == null)
					continue;

				if (o is Component)
					Object.Destroy (((Component)o).gameObject);
				else
					Object.Destroy (o);
			}

			instance.objectLookup.Clear();
			instance.poolCountLookup.Clear();
			instance.prefabLookup.Clear();
		}

		public static void CreatePool<T>(T prefab, int maxCount = 100) where T : Object
		{
			if (!instance.objectLookup.ContainsKey(prefab)) {
				instance.objectLookup.Add(prefab, new List<Object>());
				instance.poolCountLookup.Add (prefab, maxCount);
			}
		}

		static Object SpawnGameObject(Object prefab, Vector3 position, Quaternion rotation)
		{
			if (instance.objectLookup.ContainsKey(prefab))
			{
				GameObject obj = null;
				var list = instance.objectLookup[prefab];
				if (list.Count > 0)
				{
					while (obj == null && list.Count > 0)
					{
						obj = (GameObject)list[0];
						list.RemoveAt(0);
					}
					if (obj != null)
					{
//						obj.transform.parent = null;
						obj.transform.SetParent (null, false);
						obj.transform.localPosition = position;
						obj.transform.localRotation = rotation;
						obj.SetActive(true);
						instance.prefabLookup.Add(obj, prefab);
						var poolable = obj.GetComponent<Poolable> ();
						if (poolable != null) poolable.OnDepool ();
						return (Object)obj;
					}
				}
				obj = (GameObject)Object.Instantiate(prefab, position, rotation);
				instance.prefabLookup.Add(obj, prefab);
				return (Object)obj;
			}
			else
			{
				var poolable = ((GameObject)prefab).GetComponent<Poolable> ();
				if(poolable != null)
				{
					ObjectPool.CreatePool(prefab, poolable.maxCount);
					return SpawnGameObject (prefab, position, rotation);
				}
				else
					return Object.Instantiate(prefab, position, rotation);
			}
		}

		static Object SpawnComponent(Object prefab, Vector3 position, Quaternion rotation)
		{
			if (instance.objectLookup.ContainsKey(prefab))
			{
				Component obj = null;
				var list = instance.objectLookup[prefab];
				if (list.Count > 0)
				{
					while (obj == null && list.Count > 0)
					{
						obj = (Component)list[0];
						list.RemoveAt(0);
					}
					if (obj != null)
					{
//						obj.transform.parent = null;
						obj.transform.SetParent (null, false);
						obj.transform.localPosition = position;
						obj.transform.localRotation = rotation;
						obj.gameObject.SetActive(true);
						instance.prefabLookup.Add(obj, prefab);
						var poolable = obj.GetComponent<Poolable> ();
						if (poolable != null) poolable.OnDepool ();
						return (Object)obj;
					}
				}
				obj = (Component)Object.Instantiate(prefab, position, rotation);
				instance.prefabLookup.Add(obj, prefab);
				return (Object)obj;
			}
			else
			{
				var poolable = ((Component)prefab).GetComponent<Poolable> ();
				if(poolable != null)
				{
					ObjectPool.CreatePool(prefab, poolable.maxCount);
					return SpawnComponent (prefab, position, rotation);
				}
				else
					return Object.Instantiate(prefab, position, rotation);
			}
		}

		public static T Spawn<T>(T prefab, Vector3 position, Quaternion rotation) where T : Object
		{
			if(prefab is GameObject)
			{
				return (T)SpawnGameObject(prefab, position, rotation);
			}
			else if(prefab is Component)
			{
				return (T)SpawnComponent(prefab, position, rotation);
			}
			else
			{
				return (T)Object.Instantiate(prefab, position, rotation);
			}
		}
		public static T Spawn<T>(T prefab, Vector3 position) where T : Object
		{
			return Spawn(prefab, position, Quaternion.identity);
		}
		public static T Spawn<T>(T prefab) where T : Object
		{
			return Spawn(prefab, Vector3.zero, Quaternion.identity);
		}

		static void RecycleGameObject(Object obj)
		{
			GameObject o = (GameObject)obj;

			if (instance.prefabLookup.ContainsKey(o))
			{
				var prefab = instance.prefabLookup[o];
				if (instance.poolCountLookup[prefab] > instance.objectLookup[prefab].Count) {
					instance.objectLookup[prefab].Add(o);
					instance.prefabLookup.Remove(o);
//					o.transform.parent = instance.transform;
					o.transform.SetParent (instance.transform, false);
					var poolable = o.GetComponent<Poolable> ();
					if (poolable != null) poolable.OnPool ();
				} else {
					Object.Destroy (o);
				}
			}
			else
				Object.Destroy(o);
		}

		static void RecycleComponent(Object obj)
		{
			Component c = (Component)obj;

			if (instance.prefabLookup.ContainsKey(c))
			{
				var prefab = instance.prefabLookup[c];
				if (instance.poolCountLookup[prefab] > instance.objectLookup[prefab].Count) {
					instance.objectLookup[instance.prefabLookup[c]].Add(c);
					instance.prefabLookup.Remove(c);
//					c.transform.parent = instance.transform;
					c.transform.SetParent (instance.transform, false);
					var poolable = c.GetComponent<Poolable> ();
					if (poolable != null) poolable.OnPool ();
				} else {
					Object.Destroy(c);
				}
			}
			else
				Object.Destroy(c);
		}

		public static void Recycle<T>(T obj) where T : Object
		{
			if(obj is GameObject)
			{
				RecycleGameObject(obj);
			}
			else if(obj is Component)
			{
				RecycleComponent(obj);
			}
			else
			{
				Object.Destroy(obj);
			}
		}

		public static int Count<T>(T prefab) where T : Object
		{
			if (instance.objectLookup.ContainsKey(prefab))
				return instance.objectLookup[prefab].Count;
			else
				return 0;
		}

		public static ObjectPool instance
		{
			get
			{
				if (_instance != null)
					return _instance;
				var obj = new GameObject("_ObjectPool");
				obj.transform.localPosition = Vector3.zero;
				_instance = obj.AddComponent<ObjectPool>();

				DontDestroyOnLoad (obj);

				return _instance;
			}
		}
	}

	public static class ObjectPoolExtensions
	{
		public static void CreatePool<T>(this T prefab) where T : Object
		{
			ObjectPool.CreatePool(prefab);
		}
		
		public static T Spawn<T>(this T prefab, Vector3 position, Quaternion rotation) where T : Object
		{
			return ObjectPool.Spawn(prefab, position, rotation);
		}
		public static T Spawn<T>(this T prefab, Vector3 position) where T : Object
		{
			return ObjectPool.Spawn(prefab, position, Quaternion.identity);
		}
		public static T Spawn<T>(this T prefab) where T : Object
		{
			return ObjectPool.Spawn(prefab, Vector3.zero, Quaternion.identity);
		}
		
		public static void Recycle<T>(this T obj) where T : Object
		{
			ObjectPool.Recycle(obj);
		}

		public static int Count<T>(T prefab) where T : Object
		{
			return ObjectPool.Count(prefab);
		}
	}

}