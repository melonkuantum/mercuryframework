﻿using UnityEngine;
using System.Collections;

namespace Mercury.Pooling {

	[AddComponentMenu("Pool/Poolable")]
	public class Poolable : MonoBehaviour {
		public int maxCount;
		public bool awakeOnDepool;
		public bool startOnDepool;
		public bool destroyOnPool;

		public void OnPool () {
			if (destroyOnPool) {
				BroadcastMessage ("OnDestroy", SendMessageOptions.DontRequireReceiver);
			}
		}

		public void OnDepool () {
			if (awakeOnDepool) {
				BroadcastMessage ("Awake", SendMessageOptions.DontRequireReceiver);
			}

			if (startOnDepool) {
				BroadcastMessage ("Start", SendMessageOptions.DontRequireReceiver);
			}
		}

		void OnDestroy () {
			gameObject.SetActive (false);
		}
	}

}