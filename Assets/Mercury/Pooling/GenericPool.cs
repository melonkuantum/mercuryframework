﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Mercury.Pooling {
	
	public class GenericPool<T> : IPool<T> where T : class, IPoolable {

		Stack<T> m_pool;

		public GenericPool () {
			m_pool = new Stack<T> ();
		}

		public T Obtain () {
			if (m_pool.Count > 0) {
				var obj = m_pool.Pop ();
				obj.Reset ();

				return obj;
			}

			return null;
		}

		public void Store (T poolable) {
			m_pool.Push (poolable);
		}

	}

}