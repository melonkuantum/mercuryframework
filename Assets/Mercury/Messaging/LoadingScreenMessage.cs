﻿using UnityEngine;
using System.Collections;

namespace Mercury.Messaging {

	public enum LoadingScreenType {
		FullScreen,
		OnScreen
	}

	public class ShowLoadingMessage : BaseMessage {
		static ShowLoadingMessage s_instance = new ShowLoadingMessage ();
		public static ShowLoadingMessage Instance { get { return s_instance; } }

		public LoadingScreenType loadingScreenType;

		private ShowLoadingMessage () : base () {
			autoRelease = false;
		}
	}

	public class HideLoadingMessage : BaseMessage {
		static HideLoadingMessage s_instance = new HideLoadingMessage ();
		public static HideLoadingMessage Instance { get { return s_instance; } }

		public LoadingScreenType loadingScreenType;

		private HideLoadingMessage () : base () {
			autoRelease = false;
		}
	}

}