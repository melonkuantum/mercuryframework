﻿using UnityEngine;
using System.Collections;

using Mercury.Pooling;

namespace Mercury.Messaging {

	public abstract class BaseMessage : IPoolable {
		
		public enum PropagationDirection { Forward, Backward }

		string m_name;
		public string Name {
			get {
				return m_name;
			}
		}

		public bool autoRelease = true;

		public PropagationDirection direction = PropagationDirection.Forward;

		public BaseMessage () { m_name = this.GetType ().Name; }

		public virtual void Reset () {}
		public virtual void Release () {}

	}

}