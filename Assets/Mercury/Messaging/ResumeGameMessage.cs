﻿using UnityEngine;
using System.Collections;

using Mercury.Messaging;

public class ResumeGameMessage : BaseMessage {

	static ResumeGameMessage s_instance = new ResumeGameMessage ();
	public static ResumeGameMessage Instance { get { return s_instance; } }

	public ResumeGameMessage () : base () {
		autoRelease = false;
	}

}
