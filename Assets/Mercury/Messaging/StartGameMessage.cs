﻿using UnityEngine;
using System.Collections;

using Mercury.Pooling;

namespace Mercury.Messaging {

	public class StartGameMessage : BaseMessage {
		
		static GenericPool<StartGameMessage> s_pool = new GenericPool<StartGameMessage> ();
		public static StartGameMessage Obtain () {
			var msg = s_pool.Obtain ();
			if (msg == null) msg = new StartGameMessage ();

			return msg;
		}

		public BaseGameMode gameMode;

		public StartGameMessage () : base () {
		}

		public override void Release () {
			gameMode = null;

			s_pool.Store (this);
		}

	}

}