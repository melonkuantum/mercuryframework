﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using Mercury.Messaging;
using Mercury.Util;

[CustomPropertyDrawer (typeof (MessageTypeAttribute))]
public class MessageTypeAttributeDrawer : PropertyDrawer {

	public struct DrawerValuePair {
		public string str;
		public SerializedProperty property;

		public DrawerValuePair (string val, SerializedProperty property) {
			this.str = val;
			this.property = property;
		}
	}

	void HandleSelect (object menuItemObject) {
		var clickedItem = (DrawerValuePair)menuItemObject;
		clickedItem.property.stringValue = clickedItem.str;
		clickedItem.property.serializedObject.ApplyModifiedProperties ();
	}

	void PopulateMenu (GenericMenu menu, SerializedProperty property) {
		var types = ReflectiveEnumerator.GetTypes<BaseMessage> ();

		menu.AddItem (new GUIContent ("<none>"), property.stringValue == string.Empty, HandleSelect, new DrawerValuePair (string.Empty, property));

		foreach (var t in types) {
			string name = t.Name;
			menu.AddItem (new GUIContent (name), name == property.stringValue, HandleSelect, new DrawerValuePair (name, property));
		}
	}

	void Selector (SerializedProperty property) {
		var menu = new GenericMenu ();
		PopulateMenu (menu, property);
		menu.ShowAsContext ();
	}

	void DrawTypeField (Rect position, SerializedProperty property, GUIContent label) {
		position = EditorGUI.PrefixLabel (position, label);

		var propertyStringValue = property.hasMultipleDifferentValues ? new string ('\u2014', 1) : property.stringValue;
		if (GUI.Button (position, string.IsNullOrEmpty (propertyStringValue) ? string.Empty : propertyStringValue, EditorStyles.popup))
			Selector (property);
	}

	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
		if (property.propertyType == SerializedPropertyType.String) {
			DrawTypeField (position, property, label);
		} else {
			EditorGUI.LabelField(position, "ERROR:", "May only apply to type string");
		}
	}

}
