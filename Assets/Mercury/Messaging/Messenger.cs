﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Mercury.Helper;

namespace Mercury.Messaging {

	public class Messenger : SingletonComponent<Messenger> {

		public delegate bool MessageHandler (BaseMessage message);

		public static Messenger Instance {
			get { return m_isAlive ? (Messenger)_Instance : null; }
		}

		private static bool m_isAlive = true;
		
		void OnDestroy () { m_isAlive = false; }
		void OnApplicationQuit () { m_isAlive = false; }

		Dictionary<string, List<MessageHandler>> m_listeners = new Dictionary<string, List<MessageHandler>> ();
		Dictionary<BaseMessage, float> m_delayTable = new Dictionary<BaseMessage, float> ();
		Queue<BaseMessage> m_toBeQueued = new Queue<BaseMessage> ();
		Queue<BaseMessage> m_toBeCalculated = new Queue<BaseMessage> ();
		Queue<BaseMessage> m_messageQueue = new Queue<BaseMessage> ();

		public float maxQueueProcessingTime = 0.03333f;

		void Update () {
			if (m_delayTable.Count > 0) {
				foreach (var kvp in m_delayTable) {
					if (kvp.Value <= 0)
						m_toBeQueued.Enqueue (kvp.Key);
					else
						m_toBeCalculated.Enqueue (kvp.Key);
				}

				while (m_toBeQueued.Count > 0) {
					var msg = m_toBeQueued.Dequeue ();
					m_delayTable.Remove (msg);
					m_messageQueue.Enqueue (msg);
				}

				while (m_toBeCalculated.Count > 0) {
					var msg = m_toBeCalculated.Dequeue ();
					m_delayTable [msg] -= Time.unscaledDeltaTime;
				}
			}

			var timer = 0f;
			var start = Time.realtimeSinceStartup;
			while (m_messageQueue.Count > 0) {
				if (maxQueueProcessingTime > 0f &&
				    maxQueueProcessingTime < timer)
					return;

				var msg = m_messageQueue.Dequeue ();
				TriggerMessage (msg);

				timer = Time.realtimeSinceStartup - start;
			}
		}

		public void TriggerMessage (BaseMessage msg) {
			var msgName = msg.Name;
			if (!m_listeners.ContainsKey (msgName))
				return;

			var listenerList = m_listeners [msgName];

			if (msg.direction == BaseMessage.PropagationDirection.Forward) {
				for (int i = 0; i < listenerList.Count; i++) {
					if (listenerList [i] (msg)) {
						if (msg.autoRelease)
							msg.Release ();
						
						return;
					}
				}
			} else {
				for (int i = listenerList.Count-1; i >= 0; i--) {
					if (listenerList [i] (msg)) {
						if (msg.autoRelease)
							msg.Release ();
						
						return;
					}
				}
			}

			if (msg.autoRelease)
				msg.Release ();
		}

		public bool QueueMessage (BaseMessage message, float delay = 0) {
			if (message == null)
				return false;

			if (!m_listeners.ContainsKey (message.Name))
				return false;

			if (delay > 0) {
				if (m_delayTable.ContainsKey (message))
					m_delayTable [message] = delay;
				else
					m_delayTable.Add (message, delay);
			} else {
				m_messageQueue.Enqueue (message);
			}

			return true;
		}

		public bool Subscribe (System.Type type, MessageHandler handler) {
			if (!type.IsSubclassOf (typeof(BaseMessage)))
				return false;

			var msgName = type.Name;

			if (!m_listeners.ContainsKey (msgName))
				m_listeners.Add (msgName, new List<MessageHandler> ());

			var listenerList = m_listeners [msgName];
			if (listenerList.Contains (handler))
				return false;

			listenerList.Add (handler);
			return true;
		}

		public bool Subscribe<T> (MessageHandler handler) where T : BaseMessage {
			return Subscribe (typeof(T), handler);
		}

		public bool Unsubscribe (System.Type type, MessageHandler handler) {
			if (!type.IsSubclassOf (typeof(BaseMessage)))
				return false;

			var msgName = type.Name;

			if (!m_listeners.ContainsKey (msgName))
				return true;

			var listenerList = m_listeners [msgName];
			return listenerList.Remove (handler);
		}

		public bool Unsubscribe<T> (MessageHandler handler)where T : BaseMessage {
			return Unsubscribe (typeof(T), handler);
		}

	}

}