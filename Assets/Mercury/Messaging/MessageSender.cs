﻿using UnityEngine;
using System.Collections;

using FullInspector;

namespace Mercury.Messaging {

	public class MessageSender : BaseBehavior {
		public BaseMessage messageToSend;
		public bool immediate;

		public void Send () {
			if (messageToSend == null)
				return;

			messageToSend.autoRelease = false;

			if (immediate)
				Messenger.Instance.TriggerMessage (messageToSend);
			else
				Messenger.Instance.QueueMessage (messageToSend);
		}

		public void SendIfTrue (bool status) {
			if (status)
				Send ();
		}

		public void SendIfFalse (bool status) {
			if (!status)
				Send ();
		}
	}

}