﻿using UnityEngine;
using System.Collections;

using Mercury.Pooling;
using Mercury.UI;

namespace Mercury.Messaging {

	public class GoToMenuMessage : BaseMessage {
		static GenericPool<GoToMenuMessage> s_pool = new GenericPool<GoToMenuMessage> ();
		public static GoToMenuMessage Obtain () {
			var msg = s_pool.Obtain ();
			if (msg == null) msg = new GoToMenuMessage ();

			return msg;
		}

		public BaseMenu targetMenu;
		public System.Action<GameObject> onShow;
		public object[] parameters;

		public GoToMenuMessage () : base () {
		}

		public override void Release () {
			targetMenu = null;
			onShow = null;
			parameters = null;

			s_pool.Store (this);
		}
	}

}