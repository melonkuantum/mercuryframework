﻿using UnityEngine;
using System.Collections;

namespace Mercury.Messaging {

	public class BackButtonMessage : BaseMessage {

		static BackButtonMessage s_instance = new BackButtonMessage ();
		public static BackButtonMessage Instance { get { return s_instance; } }

		public BackButtonMessage () : base () {
			direction = PropagationDirection.Backward;
			autoRelease = false;
		}

	}

}