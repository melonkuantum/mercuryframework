﻿using UnityEngine;
using System.Collections;

using Mercury.Messaging;

public class PauseGameMessage : BaseMessage {
	static PauseGameMessage s_instance = new PauseGameMessage ();
	public static PauseGameMessage Instance { get { return s_instance; } }

	public PauseGameMessage () : base () {
		autoRelease = false;
	}
}
