﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Security.Cryptography;
using FullInspector;

namespace Mercury.Util.Storage {

	public static class Storage {

		static string s_path = null;
		public static string PersistentDataPath {
			get {
				if (s_path == null) {
					#if UNITY_ANDROID && !UNITY_EDITOR
					try {
						System.IntPtr obj_context = AndroidJNI.FindClass("android/content/ContextWrapper");
						System.IntPtr method_getFilesDir = AndroidJNIHelper.GetMethodID(obj_context, "getFilesDir", "()Ljava/io/File;");

						using (AndroidJavaClass cls_UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) {
							using (AndroidJavaObject obj_Activity = cls_UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity")) {
								System.IntPtr file = AndroidJNI.CallObjectMethod(obj_Activity.GetRawObject(), method_getFilesDir, new jvalue[0]);
								System.IntPtr obj_file = AndroidJNI.FindClass("java/io/File");
								System.IntPtr method_getAbsolutePath = AndroidJNIHelper.GetMethodID(obj_file, "getAbsolutePath", "()Ljava/lang/String;");   

								s_path = AndroidJNI.CallStringMethod(file, method_getAbsolutePath, new jvalue[0]);                    

								if(s_path != null) {
									Debug.Log("Got internal path: " + s_path);
								} else {
									Debug.Log("Using fallback path");
									s_path = "/data/data/" + BundleVersion.bundleIdentifier + "/files";
								}
							}
						}
					}
					catch(System.Exception e) {
						Debug.Log(e.ToString());
					}
					#else
					s_path = Application.persistentDataPath;
					#endif
				}

				return s_path;
			}
		}

		static IStorageService m_storageService;
		public static IStorageService StorageService {
			get {
				if (m_storageService == null)
					m_storageService = new FullSerializerStorageService ();

				return m_storageService;
			}
		}

	#region File Manipulation

		public static string BuildPath (params string[] strings) {
			System.Text.StringBuilder sb = new System.Text.StringBuilder ();

			foreach (string s in strings) {
				if (sb.Length > 0) {
					sb.Append ('/');
				}
				sb.Append (s);
			}

			return sb.ToString ();
		}

		public static void CreateDirectory (string path, string prefix = null) {
			Directory.CreateDirectory (BuildPath (prefix ?? PersistentDataPath, path.Substring (0, path.LastIndexOf ('/')+1)));
		}

		public static string GetFileName (string path, string prefix = null) {
			string fullPath = BuildPath (prefix ?? PersistentDataPath, path);
			return Path.GetFileName (fullPath);
		}

		public static bool Exists (string path, string prefix = null) {
			string fullPath = BuildPath (prefix ?? PersistentDataPath, path);
			return File.Exists (fullPath);
		}

		public static void Delete (string path, string prefix = null) {
			string fullPath = BuildPath (prefix ?? PersistentDataPath, path);
			File.Delete (fullPath);
		}

		public static string[] GetFiles (string path, string namePattern, SearchOption option, bool fileNameOnly, string prefix = null) {
			string fullPath = BuildPath (prefix ?? PersistentDataPath, path);

			string[] fileList = Directory.GetFiles (fullPath, namePattern, option);

			if (fileNameOnly) {
				for (int i = 0; i < fileList.Length; i++) {
					fileList[i] = Path.GetFileName (fileList[i]);
				}
			}

			return fileList;
		}

		public static string[] GetFiles (string path, string namePattern, bool fileNameOnly) {
			return GetFiles (path, namePattern, SearchOption.TopDirectoryOnly, fileNameOnly);
		}

		public static string[] GetFiles (string path, string namePattern, SearchOption option) {
			return GetFiles (path, namePattern, option, false);
		}

		public static string[] GetFiles (string path, string namePattern) {
			return GetFiles (path, namePattern, SearchOption.TopDirectoryOnly, false);
		}

		public static string[] GetFiles (string path) {
			return GetFiles (path, "*", SearchOption.TopDirectoryOnly, false);
		}

	#endregion

		public static void Write (string path, byte[] bytes) {
			CreateDirectory (path);

			string fullPath = BuildPath (PersistentDataPath, path);
			StorageService.Write (fullPath, bytes);
		}
		public static void Write (this byte[] bytes, string path) {
			Write (path, bytes);
		}

		public static byte[] Read (string path) {
			var fullPath = BuildPath (PersistentDataPath, path);
			return StorageService.Read (fullPath);
		}

		public static byte[] ReadFromStreamingAssets (string path) {
			return StorageService.ReadFromStreamingAssets (path);
		}

		public static void Serialize<T> (string path, T content) {
			CreateDirectory (path);

			var fullPath = BuildPath (PersistentDataPath, path);
			StorageService.Serialize (content, fullPath);
		}
		
		public static T Deserialize<T> (string path, T defaultValue = default (T)) {
			var fullPath = BuildPath (PersistentDataPath, path);
			return StorageService.Deserialize<T> (fullPath, defaultValue);
		}
	}

}