﻿using UnityEngine;
using System.Collections;
using System.IO;

using FullInspector;
using Mercury.Util.Security;

namespace Mercury.Util.Storage {

	public class FullSerializerStorageService : IStorageService {
		public bool useAntiCheat;

		public FullSerializerStorageService (bool useAntiCheat) {
			this.useAntiCheat = useAntiCheat;
		}

		public FullSerializerStorageService () : this (true) {
		}

		public void Write (string path, byte[] bytes) {
			if (bytes == null)
				return;
			
			using (FileStream fs = new FileStream (path, FileMode.Create, FileAccess.Write))
				fs.Write (bytes, 0, bytes.Length);
		}

		public byte[] Read (string path) {
			try {
				return File.ReadAllBytes (path);
			} catch {
				return null;
			}
		}

		public byte[] ReadFromStreamingAssets (string path) {
			byte[] bytes = null;
			
#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
			string dbpath = "file://" + Application.streamingAssetsPath + "/" + path;
			WWW www = new WWW(dbpath);
	//		yield return www;
			while (!www.isDone);
			bytes = www.bytes;
#elif UNITY_WEBPLAYER
			string dbpath = "StreamingAssets/" + path;
			WWW www = new WWW(dbpath);
	//		yield return www;
			while (!www.isDone);
			bytes = www.bytes;
#elif UNITY_IPHONE
			string dbpath = Application.dataPath + "/Raw/" + path;
			try {
				using ( FileStream fs = new FileStream(dbpath, FileMode.Open, FileAccess.Read, FileShare.Read) ) {
					bytes = new byte[fs.Length];
					fs.Read(bytes,0,(int)fs.Length);
				}
			} catch (System.Exception e) {
				Debug.LogError (e);
			}
#elif UNITY_ANDROID
			string dbpath = Application.streamingAssetsPath + "/" + path;
			WWW www = new WWW(dbpath);
	//		yield return www;
			while (!www.isDone);
			bytes = www.bytes;
#endif
			
			return bytes;
		}

		public void Serialize<T> (T content, string path) {
			string serialized = SerializationHelpers.SerializeToContent<T, FullSerializerSerializer> (content);

			if (useAntiCheat)
				serialized = AntiCheat.Encrypt (serialized);

			var bytes = System.Text.Encoding.UTF8.GetBytes (serialized);
			Write (path, bytes);
		}

		public T Deserialize<T> (string path, T defaultValue = default (T)) {
			byte[] serializedBytes = Read (path);

			if (serializedBytes == null ||
				serializedBytes.Length == 0)
				return defaultValue;

			var serialized = System.Text.Encoding.UTF8.GetString (serializedBytes);
			if (useAntiCheat)
				serialized = AntiCheat.Decrypt (serialized);

			return SerializationHelpers.DeserializeFromContent<T, FullSerializerSerializer> (serialized);
		}
	}

}