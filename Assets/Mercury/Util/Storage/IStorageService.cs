﻿
namespace Mercury.Util.Storage {

	public interface IStorageService {
		void Write (string path, byte[] bytes);
		byte[] Read (string path);
		byte[] ReadFromStreamingAssets (string path);
		void Serialize<T> (T content, string path);
		T Deserialize<T> (string path, T defaultValue = default (T));
	}

}