﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Mercury.Util {
	
	public static class ReflectiveEnumerator {
		static ReflectiveEnumerator() { }

//		public static IEnumerable<T> GetEnumerableOfType<T>(params object[] constructorArgs) where T : class, IComparable<T>
//		{
//			List<T> objects = new List<T>();
//			foreach (Assembly asm in AppDomain.CurrentDomain.GetAssemblies ()) {
//				foreach (Type type in asm.GetTypes().Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(T)))) {
//					objects.Add ((T)Activator.CreateInstance (type, constructorArgs));
//				}
//			}
//			objects.Sort();
//			return objects;
//		}

		public static IEnumerable<Type> GetTypes<T> () where T : class {
			List<Type> objects = new List<Type>();
			foreach (Assembly asm in AppDomain.CurrentDomain.GetAssemblies ()) {
				foreach (Type type in asm.GetTypes().Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(T)))) {
					objects.Add (type);
				}
			}
			objects.Sort ((t1, t2) => {
				return string.Compare (t1.Name, t2.Name);
			});
			return objects;
		}

	}

}