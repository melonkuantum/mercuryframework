﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using Mercury.Helper;

namespace Mercury.Util {
	
	public static class ResourceManager {
		
		static Dictionary<string, Object> s_loadedAssets = new Dictionary<string, Object> ();

		public static Object Load (string path) {
			if (string.IsNullOrEmpty (path))
				return null;

			Object res = null;
			if (s_loadedAssets.TryGetValue (path, out res)) {
				return res;
			} else {
				res = Resources.Load (path);

				if (res != null)
					s_loadedAssets.Add (path, res);

				return res;
			}
		}

		static IEnumerator LoadAsyncCo (string path, System.Action<Object> callback) {
			yield return null;
			
			Object res = null;
			if (s_loadedAssets.TryGetValue (path, out res)) {
				callback (res);
			} else {
				res = Resources.Load (path);
				callback (res);
	//			var asyncOp = Resources.LoadAsync (path);
	//			yield return asyncOp;
	//			
	//			res = asyncOp.asset;
	//			
	//			if (res != null)
	//				loadedAssets.Add (path, res);
	//			
	//			callback (res);
			}
		}

		public static Coroutine LoadAsync (string path, System.Action<Object> callback) {
			if (string.IsNullOrEmpty (path))
				return null;

			return CoroutineManager.StartCoroutineDelegate (LoadAsyncCo (path, callback));
		}

		public static T Load<T> (string path) where T : Object {
			if (string.IsNullOrEmpty (path))
				return null;

			Object res = null;
			if (s_loadedAssets.TryGetValue (path, out res)) {
				return (T)res;
			} else {
				res = Resources.Load<T> (path);

				if (res != null)
					s_loadedAssets.Add (path, res);

				return (T)res;
			}
		}

		static IEnumerator LoadAsyncCoGeneric<T> (string path, System.Action<T> callback) where T : Object {
			yield return null;

			Object res = null;
			if (s_loadedAssets.TryGetValue (path, out res)) {
				callback (res as T);
			} else {
				res = Resources.Load<T> (path);
				callback (res as T);
	//			var asyncOp = Resources.LoadAsync<T> (path);
	//			yield return asyncOp;
	//			
	//			res = asyncOp.asset;
	//			
	//			if (res != null)
	//				loadedAssets.Add (path, res);
	//			
	//			callback (res as T);
			}
		}

		public static Coroutine LoadAsync<T> (string path, System.Action<T> callback) where T : Object {
			if (string.IsNullOrEmpty (path))
				return null;

			return CoroutineManager.StartCoroutineDelegate (LoadAsyncCoGeneric<T> (path, callback));
		}

		public static Sprite LoadSpriteSlice (string atlasPath, string sliceName) {
			if (string.IsNullOrEmpty (sliceName)) return null;

			Object res = null;
			if (s_loadedAssets.TryGetValue (sliceName, out res)) {
				return (Sprite)res;
			} else {
				if (string.IsNullOrEmpty (atlasPath))
					return Load<Sprite> (sliceName);

				var sprites = Resources.LoadAll<Sprite> (atlasPath);

				for (int i = 0; i < sprites.Length; i++)
					s_loadedAssets.Add (sprites[i].name, sprites[i]);

				if (s_loadedAssets.TryGetValue (sliceName, out res))
					return (Sprite)res;
				else
					return null;
			}
		}

		public static void LoadAssetsFromBundle (AssetBundle bundle, params System.Type[] typesToLoad) {
			if (typesToLoad == null || typesToLoad.Length == 0)
				return;
			
			if (bundle != null) {
				for (int i = 0; i < typesToLoad.Length; i++) {
					var typeToLoad = typesToLoad [i];
					var loadedAssets = bundle.LoadAllAssets (typeToLoad);
					for (int j = 0; j < loadedAssets.Length; j++) {
						var asset = loadedAssets [j];
						if (s_loadedAssets.ContainsKey (asset.name)) {
							Object.Destroy (s_loadedAssets [asset.name]);
							s_loadedAssets [asset.name] = asset;
						} else {
							s_loadedAssets.Add (asset.name, asset);
						}
					}
				}
			}
		}

		public static void LoadAssetsFromBundle (string path, params System.Type[] typesToLoad) {
			if (typesToLoad == null || typesToLoad.Length == 0)
				return;

			if (File.Exists (path)) {
				var bundle = AssetBundle.LoadFromFile (path);

				if (bundle != null) {
					LoadAssetsFromBundle (bundle, typesToLoad);

					bundle.Unload (false);
					Object.Destroy (bundle);
				}
			}
		}

		public static AsyncOperation UnloadUnusedAssets () {
			s_loadedAssets.Clear ();
			return Resources.UnloadUnusedAssets ();
		}

		public static AsyncOperation UnloadAsset (params string[] paths) {
			if (paths != null && paths.Length > 0) {
				for (int i = 0; i < paths.Length; i++) {
					if (paths [i] != null)
						s_loadedAssets.Remove (paths [i]);
				}
			}

			return Resources.UnloadUnusedAssets ();
		}

	}

}