﻿using UnityEngine;
using System.Collections;

namespace Mercury.Util {

	public static class TimeUtil {
		public static readonly System.DateTime unixEpoch = new System.DateTime (1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);

		public static double CurrentUnix {
			get {
				return (System.DateTime.UtcNow - unixEpoch).TotalSeconds;
			}
		}

		public static double CurrentUnixMillis {
			get {
				return (System.DateTime.UtcNow - unixEpoch).TotalMilliseconds;
			}
		}

		public static double ToUnix (this System.DateTime localDate) {
			var utcDate = localDate.ToUniversalTime ();
			if (utcDate < unixEpoch)
				return 0;
			else
				return (utcDate - unixEpoch).TotalSeconds;
		}

		public static double ToUnixMillis (this System.DateTime localDate) {
			var utcDate = localDate.ToUniversalTime ();
			if (utcDate < unixEpoch)
				return 0;
			else
				return (utcDate - unixEpoch).TotalMilliseconds;
		}

		public static System.DateTime FromUnix (double seconds) {
			return unixEpoch.AddSeconds (seconds).ToLocalTime ();
		}

		public static System.DateTime FromUnixMillis (double millis) {
			return unixEpoch.AddMilliseconds (millis).ToLocalTime ();
		}

		public static bool IsUniqueToday (string key, bool autoSet = true) {
			return IsUniqueOn (key, System.DateTime.Today, autoSet);
		}

		public static bool IsUniqueOn (string key, System.DateTime date, bool autoSet = true) {
			var latest = FromUnix (Storage.Storage.Deserialize<double> (key));
			
			if (autoSet) Storage.Storage.Serialize (key, date.ToUnix ());
			
			return (date - latest).Days > 0;
		}

		public static int GetDailyUniqueDelta (string key) {
			var latest = FromUnix (Storage.Storage.Deserialize<double> (key));
			return (System.DateTime.Today - latest).Days;
		}

		public static System.DateTime GetNextDailyUniqueDate (string key) {
			var delta = GetDailyUniqueDelta (key);

			if (delta > 0) {
				return System.DateTime.Today;
			} else {
				var latest = FromUnix (Storage.Storage.Deserialize<double> (key));
				return latest.AddDays (1);
			}
		}
	}

}