﻿using UnityEngine;
using System.Collections;

namespace Mercury.Util.Security {

	public static class AntiCheat {

		static IAntiCheatService m_antiCheatService;
		public static IAntiCheatService AntiCheatService {
			get {
				if (m_antiCheatService == null)
					m_antiCheatService = MercuryConfiguration.Instance.AntiCheatService;

				return m_antiCheatService;
			}
		}

		public static string Encrypt (string toEncrypt) {
			return AntiCheatService.Encrypt (toEncrypt);
		}

		public static string Decrypt (string toDecrypt) {
			return AntiCheatService.Decrypt (toDecrypt);
		}
	}

}