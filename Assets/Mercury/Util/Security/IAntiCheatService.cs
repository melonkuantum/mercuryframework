﻿using UnityEngine;
using System.Collections;

namespace Mercury.Util.Security {

	public interface IAntiCheatService {
		string Encrypt (string toEncrypt);
		string Decrypt (string toDecrypt);
	}

}