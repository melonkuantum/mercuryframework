﻿using UnityEngine;
using System.Collections;

namespace Mercury.Util.Security {

	public class DummyAntiCheatService : IAntiCheatService {
		public string Encrypt (string toEncrypt) {
			return toEncrypt;
		}

		public string Decrypt (string toDecrypt) {
			return toDecrypt;
		}
	}

}