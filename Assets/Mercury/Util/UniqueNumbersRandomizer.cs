﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Mercury.Util {

	public class UniqueNumbersRandomizer {
		HashSet<int> m_candidates;
		List<int> m_result;

		public List<int> LastResult { get { return m_result; } }

		public UniqueNumbersRandomizer () {
			m_candidates = new HashSet<int> ();
			m_result = new List<int> ();
		}

		public List<int> GetRandoms (int count, int min, int max, bool shuffle = false) {
			if (max <= min || count < 0 || 
			    // max - min > 0 required to avoid overflow
			    (count > max - min && max - min > 0))
			{
				// need to use 64-bit to support big ranges (negative min, positive max)
				throw new System.ArgumentOutOfRangeException("Range " + min + " to " + max + 
				                                             " (" + ((long)max - (long)min) + " values), or count " + count + " is illegal");
			}

			m_candidates.Clear ();
			m_result.Clear ();

			// start count values before max, and end at max
			for (int top = max - count; top < max; top++)
			{
				// May strike a duplicate.
				// Need to add +1 to make inclusive generator
				// +1 is safe even for MaxVal max value because top < max
				if (!m_candidates.Add(Random.Range (min, top + 1))) {
					// collision, add inclusive max.
					// which could not possibly have been added before.
					m_candidates.Add(top);
				}
			}

			// load them in to a list, to sort
			foreach (var i in m_candidates) m_result.Add (i);

			// shuffle the results because HashSet has messed
			// with the order, and the algorithm does not produce
			// random-ordered results (e.g. max-1 will never be the first value)
			if (shuffle) {
				for (int i = m_result.Count - 1; i > 0; i--) {
					int k = Random.Range (0, i + 1);
					int tmp = m_result [k];
					m_result [k] = m_result [i];
					m_result [i] = tmp;
				}
			}

			return m_result;
		}

	}

}