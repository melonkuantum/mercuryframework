﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Mercury.Helper;
using Mercury.Helper.StatSystem;

namespace Mercury {

	public class MercuryUpdate : SingletonComponent<MercuryUpdate> {
		private static bool s_isAlive = true;
		public static MercuryUpdate Instance { get { return s_isAlive ? (MercuryUpdate)_Instance : null; } }

		public static event System.Action<float> onGameUpdate;
		public static event System.Action<float> onUnscaledGameUpdate;

		public static BaseModifier SetScale (float scale, bool stack = true) {
			var count = ((ModifierSelector)Instance.m_timeScale.modifiers).ReplaceGroup.Count;

			if (stack || count == 0) {
				var newReplace = new ModifierReplace (scale, 0);
				if (Instance.m_timeScale.AddModifier (newReplace)) {
					Time.timeScale = Instance.m_timeScale.Value;
					return newReplace;
				}
			}

			return null;
		}

		public static BaseModifier ForceStop (bool stack = true) {
			return SetScale (0, stack);
		}

		public static void Remove (BaseModifier modifier) {
			if (Instance.m_timeScale.RemoveModifier (modifier))
				Time.timeScale = Instance.m_timeScale.Value;
		}

		public static void Reset () {
			if (Instance.m_timeScale.ClearModifiers ())
				Time.timeScale = Instance.m_timeScale.Value;
		}

		public bool forceApplyOnUpdate = false;

		BaseStat m_timeScale;

		void Awake () {
			m_timeScale = new BaseStat (1);
			Time.timeScale = m_timeScale.Value;
		}

		void OnDestroy () {
			s_isAlive = false;
		}

		void Update () {
			if (forceApplyOnUpdate)
				Time.timeScale = m_timeScale.Value;

			if (onGameUpdate != null)
				onGameUpdate (Time.deltaTime);

			if (onUnscaledGameUpdate != null)
				onUnscaledGameUpdate (Time.unscaledDeltaTime);
		}
	}

}