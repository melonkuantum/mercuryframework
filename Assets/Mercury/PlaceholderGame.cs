﻿using UnityEngine;
using System.Collections;

using Mercury.Messaging;
using Mercury.UI;

namespace Mercury {

	public class PlaceholderGame : MercuryGame {
		public BaseMenu initialMenu;

		protected override void Start () {
			base.Start ();
			Init (null);
		}

		public override void Init (System.Action callback) {
			var msg = GoToMenuMessage.Obtain ();
			msg.targetMenu = initialMenu;

			Messenger.Instance.QueueMessage (msg);
		}

		public override void Unload (System.Action callback) {
		}

		public override void ResetData () {
		}
	}

}